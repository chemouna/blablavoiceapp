package com.codingnight.blablavoiceapp;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface BlaBlaVoiceApi {
    String FIND = "/find";
    String PARSE = "/parse";

    @GET(FIND)
    Call<FindRequestResults> trips(@Query("message") String message);

    @POST(PARSE)
    @FormUrlEncoded
    Call<List<ParseResult>> parse(@Field("message") String message);
}
