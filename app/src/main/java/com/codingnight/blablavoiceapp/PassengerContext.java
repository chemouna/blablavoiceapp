package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class PassengerContext implements Parcelable {

    public static final Creator<PassengerContext> CREATOR = new Creator<PassengerContext>() {
        public PassengerContext createFromParcel(Parcel source) {
            return new PassengerContext(source);
        }

        public PassengerContext[] newArray(int size) {
            return new PassengerContext[size];
        }
    };
    private String name;
    private int age;
    private String displayNumber;

    private PassengerContext(Parcel in) {
        this.name = in.readString();
        this.age = in.readInt();
        this.displayNumber = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDisplayNumber() {
        return displayNumber;
    }

    public void setDisplayNumber(String displayNumber) {
        this.displayNumber = displayNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.age);
        dest.writeString(this.displayNumber);
    }
}
