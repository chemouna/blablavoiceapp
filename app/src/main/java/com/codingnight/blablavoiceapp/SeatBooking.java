package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import java.io.Serializable;
import java.util.Calendar;

public class SeatBooking implements Serializable, Parcelable {

    public static final Parcelable.Creator<SeatBooking> CREATOR =
        new Parcelable.Creator<SeatBooking>() {
            public SeatBooking createFromParcel(Parcel source) {
                return new SeatBooking(source);
            }

            public SeatBooking[] newArray(int size) {
                return new SeatBooking[size];
            }
        };
    private String encryptedId;
    private int nbSeats;
    private Trip trip;
    private Price unitPrice;
    private Price passengerRefund;
    private Price driverCompensation;
    private Price pricePaid;
    private Price halfCommission;
    private Price commission;
    private Price pricePaidWithoutCommission;
    private Price halfPricePaid;
    private String code;
    @Nullable private BookingStatus bookingStatus = BookingStatus.UNKNOWN;
    private boolean tripIsPassed;
    private boolean rateBack;
    private User passenger;
    private String expireDate;
    private String bookingCode;
    private User driver;
    private String offerComment;
    private SeatBookingMessage message;
    private String fileNumber;
    private OnboardCancellationValues onboardCancellationValues;
    private int selectedProviderPayment;
    private PassengerContext passengerContext;

    public SeatBooking(Seat seat) {

        this.setEncryptedId(seat.getEncryptedId());
        this.setNbSeats(seat.getNbSeats());
        this.setTrip(seat.getTrip());
        this.setUnitPrice(seat.getUnitPrice());
        this.setPassengerRefund(seat.getPassengerRefund());
        this.setDriverCompensation(seat.getDriverCompensation());
        this.setPricePaid(seat.getPricePaid());
        this.setHalfCommission(seat.getHalfCommission());
        this.setCommission(seat.getCommission());
        this.setPricePaidWithoutCommission(seat.getPricePaidWithoutCommission());
        this.setHalfPricePaid(seat.getHalfPricePaid());
        this.setCode(seat.getCode());
        this.setBookingStatus(BookingStatus.valueOf(seat.getBookingStatus()));
        this.setExpireDate(seat.getExpireDate());
        this.setBookingCode(seat.getBookingCode());
        this.setDriver(seat.getDriver());
        this.setOfferComment(seat.getOfferComment());
    }

    public SeatBooking() {
    }

    private SeatBooking(Parcel in) {
        this.encryptedId = in.readString();
        this.nbSeats = in.readInt();
        this.trip = in.readParcelable(Trip.class.getClassLoader());
        this.unitPrice = in.readParcelable(Price.class.getClassLoader());
        this.passengerRefund = in.readParcelable(Price.class.getClassLoader());
        this.driverCompensation = in.readParcelable(Price.class.getClassLoader());
        this.pricePaid = in.readParcelable(Price.class.getClassLoader());
        this.halfCommission = in.readParcelable(Price.class.getClassLoader());
        this.commission = in.readParcelable(Price.class.getClassLoader());
        this.pricePaidWithoutCommission = in.readParcelable(Price.class.getClassLoader());
        this.halfPricePaid = in.readParcelable(Price.class.getClassLoader());
        this.code = in.readString();
        this.bookingStatus = BookingStatus.values()[in.readInt()];
        this.tripIsPassed = in.readByte() != 0;
        this.rateBack = in.readByte() != 0;
        this.passenger = in.readParcelable(User.class.getClassLoader());
        this.expireDate = in.readString();
        this.bookingCode = in.readString();
        this.driver = in.readParcelable(User.class.getClassLoader());
        this.offerComment = in.readString();
        this.message = in.readParcelable(SeatBookingMessage.class.getClassLoader());
        this.fileNumber = in.readString();
        this.onboardCancellationValues =
            in.readParcelable(OnboardCancellationValues.class.getClassLoader());
        this.selectedProviderPayment = in.readInt();
        this.passengerContext = in.readParcelable(PassengerContext.class.getClassLoader());
    }

    public SeatBookingMessage getMessage() {
        return message;
    }

    public void setMessage(SeatBookingMessage message) {
        this.message = message;
    }

    public String getEncryptedId() {
        return encryptedId;
    }

    public void setEncryptedId(String encryptedId) {
        this.encryptedId = encryptedId;
    }

    public int getNbSeats() {
        return nbSeats;
    }

    public void setNbSeats(int nbSeats) {
        this.nbSeats = nbSeats;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Price getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Price unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Price getPassengerRefund() {
        return passengerRefund;
    }

    public void setPassengerRefund(Price passengerRefund) {
        this.passengerRefund = passengerRefund;
    }

    public Price getDriverCompensation() {
        return driverCompensation;
    }

    public void setDriverCompensation(Price driverCompensation) {
        this.driverCompensation = driverCompensation;
    }

    public Price getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(Price pricePaid) {
        this.pricePaid = pricePaid;
    }

    public Price getHalfCommission() {
        return halfCommission;
    }

    public void setHalfCommission(Price halfCommission) {
        this.halfCommission = halfCommission;
    }

    public Price getCommission() {
        return commission;
    }

    public void setCommission(Price commission) {
        this.commission = commission;
    }

    public Price getPricePaidWithoutCommission() {
        return pricePaidWithoutCommission;
    }

    public void setPricePaidWithoutCommission(Price pricePaidWithoutCommission) {
        this.pricePaidWithoutCommission = pricePaidWithoutCommission;
    }

    public Price getHalfPricePaid() {
        return halfPricePaid;
    }

    public void setHalfPricePaid(Price halfPricePaid) {
        this.halfPricePaid = halfPricePaid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @NonNull
    public BookingStatus getBookingStatus() {
        return bookingStatus != null ? bookingStatus : BookingStatus.UNKNOWN;
    }

    public void setBookingStatus(@Nullable BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public boolean isTripIsPassed() {
        return tripIsPassed;
    }

    public void setTripIsPassed(boolean tripIsPassed) {
        this.tripIsPassed = tripIsPassed;
    }

    public boolean isTripIsPassedSince(int minutes) {
        Calendar departure = Calendar.getInstance();
        departure.setTime(trip.getDepartureDate());
        departure.add(Calendar.MINUTE, minutes);
        Calendar now = Calendar.getInstance();
        return now.after(departure);
    }

    public User getPassenger() {
        return passenger;
    }

    public void setPassenger(User passenger) {
        this.passenger = passenger;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public boolean isRateBack() {
        return rateBack;
    }

    public void setRateBack(boolean rateBack) {
        this.rateBack = rateBack;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getOfferComment() {
        return offerComment;
    }

    public void setOfferComment(String offerComment) {
        this.offerComment = offerComment;
    }

    public OnboardCancellationValues getOnboardCancellationValues() {
        return onboardCancellationValues;
    }

    public void setOnboardCancellationValues(OnboardCancellationValues onboardCancellationValues) {
        this.onboardCancellationValues = onboardCancellationValues;
    }

    public PassengerContext getPassengerContext() {
        return passengerContext;
    }

    public void setPassengerContext(PassengerContext passengerContext) {
        this.passengerContext = passengerContext;
    }

    public int getSelectedProviderPayment() {
        return selectedProviderPayment;
    }

    public void setSelectedProviderPayment(int selectedProviderPayment) {
        this.selectedProviderPayment = selectedProviderPayment;
    }

    public boolean isNoFee() {
        return commission != null && commission.getValue() == 0f;
    }

    public boolean isActive() {
        return !(bookingStatus == BookingStatus.DRVR_CANCEL ||
            bookingStatus == BookingStatus.PSGR_CANCEL ||
            bookingStatus == BookingStatus.PSGR_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_LATE_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_CANCEL_DRVR_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_DRVR_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_DRVR_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.DRVR_APPROVAL_TIMEOUT ||
            bookingStatus == BookingStatus.DRVR_SITE_REFUSED ||
            bookingStatus == BookingStatus.DRVR_SMS_REFUSED ||
            bookingStatus == BookingStatus.PSGR_DROP);
    }

    public boolean isCardClickable() {
        return !(bookingStatus == BookingStatus.DRVR_APPROVAL_TIMEOUT ||
            bookingStatus == BookingStatus.DRVR_SITE_REFUSED ||
            bookingStatus == BookingStatus.DRVR_SMS_REFUSED ||
            bookingStatus == BookingStatus.PSGR_DROP ||
            bookingStatus == BookingStatus.DRVR_DROP_ALL ||
            bookingStatus == BookingStatus.WAIT_PAYMENT_STATUS);
    }

    public boolean isCardGrayed() {
        return bookingStatus == BookingStatus.DRVR_CANCEL ||
            bookingStatus == BookingStatus.DRVR_CANCEL_ALL ||
            bookingStatus == BookingStatus.PSGR_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_LATE_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_CANCEL_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.PSGR_LATE_CANCEL_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.PSGR_CANCEL_DRVR_FAULT ||
            bookingStatus == BookingStatus.DRVR_APPROVAL_TIMEOUT ||
            bookingStatus == BookingStatus.DRVR_SITE_REFUSED ||
            bookingStatus == BookingStatus.DRVR_SMS_REFUSED ||
            bookingStatus == BookingStatus.PSGR_DROP ||
            bookingStatus == BookingStatus.DRVR_DROP_ALL ||
            bookingStatus == BookingStatus.PSGR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_DRVR_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_DRVR_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.SUPPORT;
    }

    public boolean isTripDetailsVisible() {
        return bookingStatus == BookingStatus.BOOKED ||
            bookingStatus == BookingStatus.PSGR_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_LATE_CANCEL_PSGR_FAULT ||
            bookingStatus == BookingStatus.SUPPORT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_UNCLEAR ||
            bookingStatus == BookingStatus.PSGR_NORIDE_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.PSGR_NORIDE_DRVR_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_PSGR_FAULT ||
            bookingStatus == BookingStatus.WAIT_DRVR_APPROVAL ||
            bookingStatus == BookingStatus.DRVR_NORIDE_NOT_MY_FAULT ||
            bookingStatus == BookingStatus.DRVR_NORIDE_UNCLEAR ||
            bookingStatus == BookingStatus.CONFIRMED;
    }

    public String getFileNumber() {
        return fileNumber;
    }

    public void setFileNumber(String fileNumber) {
        this.fileNumber = fileNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.encryptedId);
        dest.writeInt(this.nbSeats);
        dest.writeParcelable(this.trip, 0);
        dest.writeParcelable(this.unitPrice, 0);
        dest.writeParcelable(this.passengerRefund, 0);
        dest.writeParcelable(this.driverCompensation, 0);
        dest.writeParcelable(this.pricePaid, 0);
        dest.writeParcelable(this.halfCommission, 0);
        dest.writeParcelable(this.commission, 0);
        dest.writeParcelable(this.pricePaidWithoutCommission, 0);
        dest.writeParcelable(this.halfPricePaid, 0);
        dest.writeString(this.code);
        dest.writeInt(getBookingStatus().ordinal());
        dest.writeByte(tripIsPassed ? (byte) 1 : (byte) 0);
        dest.writeByte(rateBack ? (byte) 1 : (byte) 0);
        dest.writeParcelable(this.passenger, 0);
        dest.writeString(this.expireDate);
        dest.writeString(this.bookingCode);
        dest.writeParcelable(this.driver, 0);
        dest.writeString(this.offerComment);
        dest.writeParcelable(this.message, 0);
        dest.writeString(this.fileNumber);
        dest.writeParcelable(this.onboardCancellationValues, 0);
        dest.writeInt(this.selectedProviderPayment);
        dest.writeParcelable(this.passengerContext, 0);
    }

    @ApiEnumFallback public enum BookingStatus {
        WAIT_DRVR_APPROVAL,
        WAIT_PAYMENT_INFO,
        WAIT_PAYMENT_STATUS,
        PSGR_DROP,
        PSGR_CANCEL,
        PSGR_LATE,
        PSGR_NORIDE,
        PSGR_NORIDE_PSGR_FAULT,
        PSGR_NORIDE_DRVR_FAULT,
        PSGR_NORIDE_NOT_MY_FAULT,
        PSGR_NORIDE_UNCLEAR,
        PSGR_CANCEL_PSGR_FAULT,
        PSGR_CANCEL_DRVR_FAULT,
        PSGR_CANCEL_NOT_MY_FAULT,
        PSGR_LATE_CANCEL_NOT_MY_FAULT,
        PSGR_LATE_CANCEL_PSGR_FAULT,
        DRVR_CANCEL,
        DRVR_CANCEL_ALL,
        DRVR_NORIDE,
        DRVR_NORIDE_DRVR_FAULT,
        DRVR_NORIDE_PSGR_FAULT,
        DRVR_NORIDE_NOT_MY_FAULT,
        DRVR_NORIDE_UNCLEAR,
        DRVR_APPROVAL_TIMEOUT,
        DRVR_DROP_ALL,
        DRVR_CONFIRMED,
        BOOKED,
        CONFIRMED,
        DRVR_SITE_REFUSED,
        DRVR_SMS_REFUSED,
        SUPPORT,
        CS_PSGR_NOTRMB_DRVR_NOTPAID,
        CS_PSGR_NOTRMB_DRVR_PAID100,
        CS_PSGR_RMB50_DRVR_PAID50,
        CS_PSGR_RMB50_DRVR_PAID100,
        CS_PSGR_RMB100_DRVR_NOTPAID,
        CS_PSGR_RMB100_DRVR_PAID50,
        CS_PSGR_RMB100_DRVR_PAID100,
        CS_PSGR_RMBFULL_DRVR_NOTPAID,
        CS_PSGR_RMBFULL_DRVR_PAID50,
        CS_PSGR_RMBFULL_DRVR_PAID100,
        CS_PSGR_REFILL_FULLCOM,
        CS_PSGR_REFILL_75COM,
        CS_PSGR_REFILL_50COM,
        CS_PSGR_NOREFILL,
        AUTO_PSGR_RMB50_DRVR_PAID50,
        UNKNOWN
    }
}