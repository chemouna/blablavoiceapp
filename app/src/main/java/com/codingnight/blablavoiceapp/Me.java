package com.codingnight.blablavoiceapp;

public class Me {

    private static volatile User instance;

    private Me() {
    }

    public static User getInstance() {
        if (null == instance) {
            synchronized (Me.class) {
                instance = new User();
            }
        }
        return instance;
    }

    public static void setMe(User user) {
        synchronized (Me.class) {
            instance = user;
        }
    }
}
