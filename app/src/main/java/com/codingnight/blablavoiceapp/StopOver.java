package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;

public class StopOver extends Place implements Parcelable {

    public static final Creator<StopOver> CREATOR = new Creator<StopOver>() {
        public StopOver createFromParcel(Parcel source) {
            return new StopOver(source);
        }

        public StopOver[] newArray(int size) {
            return new StopOver[size];
        }
    };
    private boolean departurePlace;
    private boolean arrivalPlace;

    public StopOver(@Nullable String cityName, @Nullable String address, double latitude,
        double longitude, @Nullable String countryCode) {
        super(cityName, address, latitude, longitude, countryCode);
    }

    private StopOver(Parcel in) {
        super(in);
        this.departurePlace = in.readByte() != 0;
        this.arrivalPlace = in.readByte() != 0;
    }

    public static StopOver valueOf(Place place) {
        return new StopOver(place.getCityName(), place.getAddress(), place.getLatitude(),
            place.getLongitude(), place.getCountryCode());
    }

    public static List<StopOver> valueOf(List<Place> places) {
        List<StopOver> result = new ArrayList<>();
        if (places != null) {
            for (Place place : places) {
                result.add(valueOf(place));
            }
        }
        return result;
    }

    public boolean isDeparturePlace() {
        return departurePlace;
    }

    public void setDeparturePlace(boolean departurePlace) {
        this.departurePlace = departurePlace;
    }

    public boolean isArrivalPlace() {
        return arrivalPlace;
    }

    public void setArrivalPlace(boolean arrivalPlace) {
        this.arrivalPlace = arrivalPlace;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeByte(departurePlace ? (byte) 1 : (byte) 0);
        dest.writeByte(arrivalPlace ? (byte) 1 : (byte) 0);
    }
}