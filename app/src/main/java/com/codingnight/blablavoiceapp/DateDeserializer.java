package com.codingnight.blablavoiceapp;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by g.jestin on 6/16/16.
 */

public class DateDeserializer implements JsonDeserializer<Date> {
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss";

    @Override
    public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2)
            throws JsonParseException {
        if (element.isJsonPrimitive()) {
            JsonPrimitive primitiveElement = (JsonPrimitive) element;

            // handle TTL in seconds
            if (primitiveElement.isNumber()) {
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, element.getAsInt());
                return calendar.getTime();
            }

            // handle formatted date
            if (primitiveElement.isString()) {
                return parseDate(element.getAsString(), DATE_FORMAT);
            }
        }

        return null;
    }

    private Date parseDate(String date, String format) {
        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.getDefault());

        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}
