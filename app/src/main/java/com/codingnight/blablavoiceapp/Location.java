package com.codingnight.blablavoiceapp;

public class Location {

    double lat, lng;
    transient String latLon;
    transient Integer radius;

    public Location() {
    }

    public Location(Double lat, Double lng, Integer radius) {
        setLatLon(lat, lng);
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
    }

    public String getLatLon() {
        if (latLon == null) {
            setLatLon(lat, lng);
        }
        return latLon;
    }

    public void setLatLon(Double lat, Double lon) {
        if (lat != null && lon != null) {
            this.latLon = String.format("%s,%s", lat, lon);
        }
    }

    public String getFormattedLatLon(Double lat, Double lon) {
        if (lat != null && lon != null) {
            this.latLon = String.format("%s|%s", lat, lon);
        }
        return latLon;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}