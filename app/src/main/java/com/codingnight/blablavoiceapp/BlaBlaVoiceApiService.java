package com.codingnight.blablavoiceapp;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.bind.TypeAdapters;

import java.util.Date;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BlaBlaVoiceApiService {
    private static final String API_URL = "http://gm-dev.fr:3002";

    private static final BlaBlaVoiceApiService INSTANCE = new BlaBlaVoiceApiService();

    private BlaBlaVoiceApi api;

    private BlaBlaVoiceApiService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        GsonConverterFactory gcf = GsonConverterFactory.create((new GsonBuilder())
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .setFieldNamingStrategy(new BlaBlaCarFieldNamingStrategy())
                .registerTypeAdapterFactory(TypeAdapters.ENUM_FACTORY)
                .setDateFormat("dd/MM/yyyy HH:mm:ss")
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create());

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder().client(client)
                .baseUrl(API_URL)
                .addConverterFactory(gcf)
                .build();

        api = retrofit.create(BlaBlaVoiceApi.class);
    }

    public static BlaBlaVoiceApi getApi() {
        return INSTANCE.api;
    }
}
