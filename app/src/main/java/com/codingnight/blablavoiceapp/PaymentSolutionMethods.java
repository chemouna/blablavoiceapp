package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

public class PaymentSolutionMethods implements Parcelable {

    public static final Creator<PaymentSolutionMethods> CREATOR =
        new Creator<PaymentSolutionMethods>() {
            @Override
            public PaymentSolutionMethods createFromParcel(Parcel in) {
                return new PaymentSolutionMethods(in);
            }

            @Override
            public PaymentSolutionMethods[] newArray(int size) {
                return new PaymentSolutionMethods[size];
            }
        };
    @SerializedName("id") private int id;
    @SerializedName("one_click_enabled") private boolean oneClickEnabled;
    @SerializedName("obfuscated_card_number") private String obfuscatedCardNumber;
    @SerializedName("card_expiration_month") private String cardExpirationMonth;
    @SerializedName("card_expiration_year") private String cardExpirationYear;
    @SerializedName("card_type") private String cardType;

    public PaymentSolutionMethods(int id, boolean oneClickEnabled, String obfuscatedCardNumber,
        String cardExpirationMonth, String cardExpirationYear, String cardType) {
        this.id = id;
        this.oneClickEnabled = oneClickEnabled;
        this.obfuscatedCardNumber = obfuscatedCardNumber;
        this.cardExpirationMonth = cardExpirationMonth;
        this.cardExpirationYear = cardExpirationYear;
        this.cardType = cardType;
    }

    protected PaymentSolutionMethods(Parcel in) {
        id = in.readInt();
        oneClickEnabled = in.readByte() != 0;
        this.obfuscatedCardNumber = in.readString();
        this.cardExpirationMonth = in.readString();
        this.cardExpirationYear = in.readString();
        this.cardType = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean OneClickEnabled() {
        return oneClickEnabled;
    }

    public void setOneClickEnabled(boolean oneClickEnabled) {
        this.oneClickEnabled = oneClickEnabled;
    }

    public String getObfuscatedCardNumber() {
        return obfuscatedCardNumber;
    }

    public void setObfuscatedCardNumber(String obfuscatedCardNumber) {
        this.obfuscatedCardNumber = obfuscatedCardNumber;
    }

    public String getCardExpirationMonth() {
        return cardExpirationMonth;
    }

    public void setCardExpirationMonth(String cardExpirationMonth) {
        this.cardExpirationMonth = cardExpirationMonth;
    }

    public String getCardExpirationYear() {
        return cardExpirationYear;
    }

    public void setCardExpirationYear(String cardExpirationYear) {
        this.cardExpirationYear = cardExpirationYear;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public boolean isSavedCreditCard() {
        return (!TextUtils.isEmpty(obfuscatedCardNumber)
            && !TextUtils.isEmpty(cardExpirationMonth)
            && !TextUtils.isEmpty(cardExpirationYear)
            && !TextUtils.isEmpty(cardType));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeByte((byte) (oneClickEnabled ? 1 : 0));
        dest.writeString(obfuscatedCardNumber);
        dest.writeString(cardExpirationMonth);
        dest.writeString(cardExpirationYear);
        dest.writeString(cardType);
    }
}
