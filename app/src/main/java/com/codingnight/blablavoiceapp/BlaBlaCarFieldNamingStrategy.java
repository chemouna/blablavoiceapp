package com.codingnight.blablavoiceapp;

import com.google.gson.FieldNamingStrategy;

import java.lang.reflect.Field;
import java.util.Locale;

/**
 * Created by g.jestin on 6/16/16.
 */

public class BlaBlaCarFieldNamingStrategy implements FieldNamingStrategy {
    /**
     * Converts the field name that uses camel-case define word separation into
     * separate words that are separated by the provided {@code separatorString}.
     */
    private static String separateCamelCase(String name, String separator) {
        StringBuilder translation = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            char character = name.charAt(i);
            if (Character.isUpperCase(character) && translation.length() != 0) {
                translation.append(separator);
            }
            translation.append(character);
        }
        return translation.toString();
    }

    @Override
    public String translateName(Field field) {
        return separateCamelCase(field.getName(), "_").toLowerCase(Locale.ROOT);
    }
}
