package com.codingnight.blablavoiceapp;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

public class TripBase implements Parcelable {

    public static final Creator<TripBase> CREATOR = new Creator<TripBase>() {
        public TripBase createFromParcel(Parcel source) {
            return new TripBase(source);
        }

        public TripBase[] newArray(int size) {
            return new TripBase[size];
        }
    };
    protected String permanentId;
    protected Date departureDate;
    protected Place departurePlace;
    protected Place arrivalPlace;
    protected String tripOfferEncryptedId;

    public TripBase() {
    }

    public TripBase(String permanentId, Date departureDate, Place departurePlace,
        Place arrivalPlace, String tripOfferEncryptedId) {
        this.permanentId = permanentId;
        this.departureDate = departureDate;
        this.departurePlace = departurePlace;
        this.arrivalPlace = arrivalPlace;
        this.tripOfferEncryptedId = tripOfferEncryptedId;
    }

    public TripBase(String permanentId, Date departureDate, Place departurePlace,
        Place arrivalPlace) {
        this(permanentId, departureDate, departurePlace, arrivalPlace, null);
    }

    public TripBase(TripBase other) {
        this.permanentId = other.permanentId;
        this.departureDate = other.departureDate;
        this.departurePlace = other.departurePlace;
        this.arrivalPlace = other.arrivalPlace;
        this.tripOfferEncryptedId = other.tripOfferEncryptedId;
    }

    private TripBase(Parcel in) {
        this.permanentId = in.readString();
        long tmpDepartureDate = in.readLong();
        this.departureDate = tmpDepartureDate == -1 ? null : new Date(tmpDepartureDate);
        this.departurePlace = in.readParcelable(Place.class.getClassLoader());
        this.arrivalPlace = in.readParcelable(Place.class.getClassLoader());
        this.tripOfferEncryptedId = in.readString();
    }

    public String getPermanentId() {
        return permanentId;
    }

    public void setPermanentId(String permanentId) {
        this.permanentId = permanentId;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Place getDeparturePlace() {
        return departurePlace;
    }

    public void setDeparturePlace(Place departurePlace) {
        this.departurePlace = departurePlace;
    }

    public Place getArrivalPlace() {
        return arrivalPlace;
    }

    public void setArrivalPlace(Place arrivalPlace) {
        this.arrivalPlace = arrivalPlace;
    }

    public boolean isPassed() {
        return Calendar.getInstance().getTime().after(departureDate);
    }

    public String getFormattedRouteByCityName(Context context) {
        String departure = Place.getCityName(departurePlace);
        String arrival = Place.getCityName(arrivalPlace);
        if (departure == null || arrival == null) return null;
        //return String.format(
        //    BlablacarApplication.getExtString(context, R.id.str_global_route_format_),
        //    departurePlace.getCityName(), arrivalPlace.getCityName());
        return null;
    }

    public String getTripOfferEncryptedId() {
        return tripOfferEncryptedId;
    }

    public void setTripOfferEncryptedId(String tripOfferEncryptedId) {
        this.tripOfferEncryptedId = tripOfferEncryptedId;
    }

    /**
     * Check if the trip departure date is passed by 24 hour
     */
    public Boolean isRecent() {
        if (null == this.departureDate) {

            return false;
        }
        return false;
        //return Calendar.getInstance().getTimeInMillis()
        //    < AppUtils.DAY_IN_MS + this.departureDate.getTime();
    }

    /**
     * Check if the trip departure is Passed
     */
    public Boolean isInFuture() {
        if (null == this.departureDate) {

            return false;
        }

        return Calendar.getInstance().getTimeInMillis() < this.departureDate.getTime();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.permanentId);
        dest.writeLong(departureDate != null ? departureDate.getTime() : -1);
        dest.writeParcelable(this.departurePlace, 0);
        dest.writeParcelable(this.arrivalPlace, 0);
        dest.writeString(this.tripOfferEncryptedId);
    }
}