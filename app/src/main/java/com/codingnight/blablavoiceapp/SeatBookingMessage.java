package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class SeatBookingMessage implements Parcelable {

    public static final Creator<SeatBookingMessage> CREATOR = new Creator<SeatBookingMessage>() {
        public SeatBookingMessage createFromParcel(Parcel source) {
            return new SeatBookingMessage(source);
        }

        public SeatBookingMessage[] newArray(int size) {
            return new SeatBookingMessage[size];
        }
    };
    private SeatBookingMessageReason reason;
    private String userId;

    public SeatBookingMessage() {
    }

    private SeatBookingMessage(Parcel in) {
        this.reason = in.readParcelable(SeatBookingMessageReason.class.getClassLoader());
        this.userId = in.readString();
    }

    public SeatBookingMessageReason getReason() {
        return reason;
    }

    public void setReason(SeatBookingMessageReason reason) {
        this.reason = reason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.reason, 0);
        dest.writeString(this.userId);
    }
}
