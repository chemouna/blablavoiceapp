package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ReviewUser implements Parcelable {
    public static final Creator<ReviewUser> CREATOR = new Creator<ReviewUser>() {
        @Override
        public ReviewUser createFromParcel(Parcel in) {
            return new ReviewUser(in);
        }

        @Override
        public ReviewUser[] newArray(int size) {
            return new ReviewUser[size];
        }
    };
    private final String encryptedId;
    private final String displayName;
    private final String profilePicture;

    public ReviewUser(@NonNull String encryptedId, @NonNull String displayName,
        @Nullable String profilePicture) {
        this.encryptedId = encryptedId;
        this.displayName = displayName;
        this.profilePicture = profilePicture;
    }

    private ReviewUser(Parcel in) {
        encryptedId = in.readString();
        displayName = in.readString();
        profilePicture = in.readString();
    }

    public String getEncryptedId() {
        return encryptedId;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public String getProfilePicture() {
        return profilePicture;
    }

    @NonNull
    public String getInitials() {
        if (null == displayName) {
            return "";
        }

        String[] words = displayName.split(" ");

        if (words.length > 0) {
            final StringBuilder result = new StringBuilder(words.length);
            for (String word : words) {
                if (word.length() > 0) {
                    result.append(Character.toUpperCase(word.charAt(0)));
                }
            }

            return result.toString();
        } else {
            return String.valueOf(displayName.charAt(0));
        }
    }

    public boolean isMe() {
        return encryptedId.equals(Me.getInstance().getEncryptedId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(encryptedId);
        dest.writeString(displayName);
        dest.writeString(profilePicture);
    }
}

