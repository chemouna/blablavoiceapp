package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class ExpirationRange implements Parcelable {

    public static final Creator<ExpirationRange> CREATOR = new Creator<ExpirationRange>() {
        public ExpirationRange createFromParcel(Parcel source) {
            return new ExpirationRange(source);
        }

        public ExpirationRange[] newArray(int size) {
            return new ExpirationRange[size];
        }
    };
    private String minDate;
    private String maxDate;
    private String defaultDate;

    public ExpirationRange() {
    }

    private ExpirationRange(Parcel in) {
        this.minDate = in.readString();
        this.maxDate = in.readString();
        this.defaultDate = in.readString();
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public String getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(String defaultDate) {
        this.defaultDate = defaultDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.minDate);
        dest.writeString(this.maxDate);
        dest.writeString(this.defaultDate);
    }
}
