package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class OnboardCancellationValues implements Parcelable {

    public static final Creator<OnboardCancellationValues> CREATOR =
        new Creator<OnboardCancellationValues>() {
            public OnboardCancellationValues createFromParcel(Parcel source) {
                return new OnboardCancellationValues(source);
            }

            public OnboardCancellationValues[] newArray(int size) {
                return new OnboardCancellationValues[size];
            }
        };
    private String total;
    private String half;
    private String threeQuarts;

    private OnboardCancellationValues(Parcel in) {
        this.total = in.readString();
        this.half = in.readString();
        this.threeQuarts = in.readString();
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getHalf() {
        return half;
    }

    public void setHalf(String half) {
        this.half = half;
    }

    public String getThreeQuarts() {
        return threeQuarts;
    }

    public void setThreeQuarts(String threeQuarts) {
        this.threeQuarts = threeQuarts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.total);
        dest.writeString(this.half);
        dest.writeString(this.threeQuarts);
    }
}
