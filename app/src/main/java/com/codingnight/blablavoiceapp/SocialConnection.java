package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

public class SocialConnection implements Parcelable, Serializable {

    public static final Creator<SocialConnection> CREATOR = new Creator<SocialConnection>() {
        @Override
        public SocialConnection createFromParcel(Parcel in) {
            return new SocialConnection(in);
        }

        @Override
        public SocialConnection[] newArray(int size) {
            return new SocialConnection[size];
        }
    };
    @SerializedName("isConnected") private boolean isConnected;
    private String friends;

    protected SocialConnection(Parcel in) {
        this.isConnected = in.readInt() == 1;
        this.friends = in.readString();
    }

    public boolean getIsConnected() {
        return isConnected;
    }

    public void setIsConnected(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.isConnected ? 1 : 0);
        dest.writeString(this.friends);
    }
}

