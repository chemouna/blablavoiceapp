package com.codingnight.blablavoiceapp;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class Geocode implements Serializable {

    private static final String TYPE_COUNTRY = "country";
    private static final String TYPE_LOCALITY = "locality";
    List<Result> results;

    public Result getTopResult() {

        if (results != null && results.size() > 0) {

            for (Result entry : results) {
                if (entry.hasLocation() && entry.hasLocality() && entry.hasCountryCode()) {
                    return entry;
                }
            }

            return null;
        }

        return null;
    }

    public static class Result implements Serializable {
        String formattedAddress;
        List<AddressComponent> addressComponents;
        Geometry geometry;

        public Result() {
        }

        public Result(String localityAddress, double lat, double lon, String countryCode,
            String formattedAddress) {
            addressComponents = new ArrayList<AddressComponent>();
            addressComponents.add(new AddressComponent(localityAddress, "", Geocode.TYPE_LOCALITY));
            addressComponents.add(new AddressComponent("", countryCode, Geocode.TYPE_COUNTRY));
            geometry = new Geometry(new Location(lat, lon, 0));
            this.formattedAddress = formattedAddress;
        }

        public boolean hasLocation() {
            return geometry != null && geometry.location != null;
        }

        public Location getLocation() {
            return hasLocation() ? geometry.location : null;
        }

        public boolean hasLocality() {
            return getLocality() != null;
        }

        public boolean hasCountryCode() {
            return getCountryCode() != null;
        }

        public String getFormattedAddress() {
            return formattedAddress;
        }

        public String getLocality() {
            for (AddressComponent addressComponent : addressComponents) {
                if (addressComponent.types.contains(Geocode.TYPE_LOCALITY)) {
                    return addressComponent.longName;
                }
            }
            return null;
        }

        public String getCountryCode() {
            for (AddressComponent addressComponent : addressComponents) {
                if (addressComponent.types.contains(Geocode.TYPE_COUNTRY)) {
                    return addressComponent.shortName;
                }
            }
            return null;
        }
    }

    public static class AddressComponent implements Serializable {
        String longName;
        String shortName;
        HashSet<String> types;

        public AddressComponent() {
        }

        public AddressComponent(String longName, String shortName, String... types) {
            this.longName = longName;
            this.shortName = shortName;
            this.types = new HashSet<String>(Arrays.asList(types));
        }
    }

    public static class Geometry implements Serializable {
        Location location;

        public Geometry() {
        }

        public Geometry(Location location) {
            this.location = location;
        }
    }
}