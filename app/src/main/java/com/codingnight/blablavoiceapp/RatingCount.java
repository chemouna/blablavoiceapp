package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Comparator;

public class RatingCount implements Comparable<RatingCount>, Parcelable {

    public static final int VALUE_UNKNOWN = -1;
    public static final int VALUE_TO_BE_AVOIDED = 1;
    public static final int VALUE_POOR = 2;
    public static final int VALUE_GOOD = 3;
    public static final int VALUE_EXCELLENT = 4;
    public static final int VALUE_EXCEPTIONALLY_AWESOME = 5;
    public static final Creator<RatingCount> CREATOR = new Creator<RatingCount>() {
        public RatingCount createFromParcel(Parcel source) {
            return new RatingCount(source);
        }

        public RatingCount[] newArray(int size) {
            return new RatingCount[size];
        }
    };
    @Value private final int value;
    private final int count;

    public RatingCount() {
        this.value = VALUE_UNKNOWN;
        this.count = -1;
    }

    public RatingCount(int value, int count) {
        this.value = value;
        this.count = count;
    }

    @SuppressWarnings("ResourceType")
    private RatingCount(Parcel in) {
        this.value = in.readInt();
        this.count = in.readInt();
    }

    public static Comparator<RatingCount> getReverseComparator() {
        return new Comparator<RatingCount>() {
            @Override
            public int compare(RatingCount first, RatingCount last) {
                return -first.compareTo(last);
            }
        };
    }

    @Value
    public int getValue() {
        return value;
    }

    public int getCount() {
        return count;
    }

    @Override
    public int compareTo(@NonNull RatingCount ratingCount) {
        return getValue() - ratingCount.getValue();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(value);
        dest.writeInt(count);
    }

    @Retention(RetentionPolicy.SOURCE) @IntDef({
        VALUE_UNKNOWN, VALUE_TO_BE_AVOIDED, VALUE_POOR, VALUE_GOOD, VALUE_EXCELLENT,
        VALUE_EXCEPTIONALLY_AWESOME
    }) public @interface Value {
    }
}

