package com.codingnight.blablavoiceapp;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FindRequestResults {

    public class ParsedTripsResults {

        private List<Trip> trips;

        public ParsedTripsResults() {}

        public List<Trip> getTrips() {
            return trips;
        }

        @Override
        public String toString() {
            return "ParsedTripsResults{" +
                    "trips=" + trips +
                    '}';
        }
    }

    @SerializedName("parsed-trips-results")
    public ParsedTripsResults data;

    public FindRequestResults() {}
}
