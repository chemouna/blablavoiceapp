package com.codingnight.blablavoiceapp;

public class StringUtils {

    /**
     * Real empty check
     *
     * @param str string to inspect
     * @return true if string is null of "", else false
     */
    public static boolean isEmpty(String str) {
        return str == null || str.equals("");
    }

    public String format(final String format, final Object... args) {
        try {
            return String.format(format, args);
        } catch (Exception ex) {
            return format;
        }
    }

    public static boolean isValidPinCode(String pinCode) {
        return pinCode.matches("[0-9]+");
    }

}