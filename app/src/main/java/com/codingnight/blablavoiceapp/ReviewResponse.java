package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import java.io.Serializable;
import java.util.ArrayList;

public class ReviewResponse implements Parcelable, Serializable {

    public static final Creator<ReviewResponse> CREATOR = new Creator<ReviewResponse>() {
        public ReviewResponse createFromParcel(Parcel source) {
            return new ReviewResponse(source);
        }

        public ReviewResponse[] newArray(int size) {
            return new ReviewResponse[size];
        }
    };
    private String response;

    public ReviewResponse(String response) {
        this.response = response;
    }

    private ReviewResponse(Parcel in) {
        this.response = in.readString();
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.response);
    }

    public static class Wrapper implements Parcelable, Serializable {
        public static final Creator<Wrapper> CREATOR = new Creator<Wrapper>() {
            public Wrapper createFromParcel(Parcel source) {
                return new Wrapper(source);
            }

            public Wrapper[] newArray(int size) {
                return new Wrapper[size];
            }
        };
        private final ArrayList<ReviewResponse> responses;

        public Wrapper() {
            this.responses = new ArrayList<>();
        }

        private Wrapper(Parcel in) {
            responses = new ArrayList<>();
            in.readTypedList(responses, ReviewResponse.CREATOR);
        }

        @NonNull
        public ArrayList<ReviewResponse> getResponses() {
            return responses != null ? responses : new ArrayList<ReviewResponse>();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeTypedList(responses);
        }
    }
}

