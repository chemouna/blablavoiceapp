package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.codingnight.blablavoiceapp.Moderation.STATUS_UNKNOWN;

public final class User extends UserBase implements Parcelable, Serializable {

    public static final int GRADE_UNKNOWN = -1;
    public static final int GRADE_BEGINNER = 0;
    public static final int GRADE_REGULAR = 1;
    public static final int GRADE_CONFIRMED = 2;
    public static final int GRADE_EXPERT = 3;
    public static final int GRADE_AMBASSADOR = 4;
    public static final String TO_CHECK = "TO_CHECK";
    public static final String CHECKED = "CHECKED";
    public static final String PENDING = "PENDING";
    public static final String NOT_APPLICABLE = "NOT_APPLICABLE";
    public static final String OPINION_YES = "_UE_YES";
    public static final String OPINION_NO = "_UE_NO";
    public static final String OPINION_MAYBE = "_UE_MAYBE";
    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
    private String encryptedId;
    private String picture;
    private boolean hasPicture;
    private int age;
    private float rating;
    private int ratingCount;
    @Opinion private String smoking;
    @Opinion private String dialog;
    @Opinion private String pets;
    @Opinion private String music;
    @Grade private int grade = GRADE_UNKNOWN;
    private Gender gender;
    private boolean phoneVerified;
    private boolean emailVerified;
    private boolean phoneHidden;
    private Integer ridesOffered;
    private Date lastLogin;
    private Date memberSince;
    private Integer responseRate;
    private List<Review> userReviews;
    private Phone phone;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
    private Integer birthyear;
    private boolean subscribeNewsletter;
    @Nullable @Moderation.Status private String pictureModerationStatus;
    private String bio;
    @Nullable @Moderation.Status private String bioModerationStatus;
    private Integer fbFriends;
    private boolean displayNameLong;
    private String idUser;
    // Available only from thread API call.
    @Nullable private Boolean ratingAuthorized;
    @Nullable private Trip.BookingType bookingType;
    private Float drivingRating;
    private Social social;
    private boolean signedCharter;
    private boolean recurringBookingEligible;
    @IdChecked private String idChecked;
    private OAuth2Information oAuth2Information;
    public User() {
    }
    public User(String displayName) {
        super(displayName);
    }
    public User(UserBase other) {
        super(other);
    }

    @SuppressWarnings("ResourceType")
    private User(Parcel in) {
        super(in);

        this.encryptedId = in.readString();
        this.picture = in.readString();
        this.hasPicture = in.readByte() != 0;
        this.age = in.readInt();
        this.rating = in.readFloat();
        this.ratingCount = in.readInt();
        this.smoking = in.readString();
        this.dialog = in.readString();
        this.pets = in.readString();
        this.music = in.readString();
        this.grade = in.readInt();
        int tmpGender = in.readInt();
        this.gender = tmpGender == -1 ? null : Gender.values()[tmpGender];
        this.phoneVerified = in.readInt() == 1;
        this.emailVerified = in.readInt() == 1;
        this.phoneHidden = in.readInt() == 1;
        this.ridesOffered = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpLastLogin = in.readLong();
        this.lastLogin = tmpLastLogin == -1 ? null : new Date(tmpLastLogin);
        long tmpMemberSince = in.readLong();
        this.memberSince = tmpMemberSince == -1 ? null : new Date(tmpMemberSince);
        this.responseRate = (Integer) in.readValue(Integer.class.getClassLoader());
        userReviews = new ArrayList<>();
        in.readTypedList(userReviews, Review.CREATOR);
        this.phone = in.readParcelable(Phone.class.getClassLoader());
        this.firstname = in.readString();
        this.lastname = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.birthyear = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subscribeNewsletter = in.readInt() == 1;
        this.pictureModerationStatus = in.readString();
        this.bio = in.readString();
        this.bioModerationStatus = in.readString();
        this.fbFriends = (Integer) in.readValue(Integer.class.getClassLoader());
        this.displayNameLong = in.readInt() == 1;
        this.idUser = in.readString();
        this.ratingAuthorized = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.oAuth2Information = in.readParcelable(OAuth2Information.class.getClassLoader());
        this.displayName = in.readString();
        this.bookingType = Trip.BookingType.values()[in.readInt()];
        this.social = in.readParcelable(Social.class.getClassLoader());
        this.signedCharter = in.readInt() == 1;
        this.idChecked = in.readString();
    }


    public Float getDrivingRating() {
        return drivingRating;
    }

    public void setDrivingRating(Float drivingRating) {
        this.drivingRating = drivingRating;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getEncryptedId() {
        return encryptedId;
    }

    public void setEncryptedId(String encryptedId) {
        this.encryptedId = encryptedId;
    }

    /**
     * Return User avatar URL.
     * If hasPicture is false, this method will return null. This in order to prevent the backend to send us a
     * placeholder.
     *
     * @return user url string or null is hasPicture is false.
     */
    @Nullable
    public String getPicture() {
        return null != picture && hasPicture ? picture : null;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean hasPicture() {
        return hasPicture;
    }

    public void setHasPicture(boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    @Nullable
    @Opinion
    public String getSmoking() {
        return smoking;
    }

    public void setSmoking(@Opinion String smoking) {
        this.smoking = smoking;
    }

    @Nullable
    @Opinion
    public String getDialog() {
        return dialog;
    }

    public void setDialog(@Opinion String dialog) {
        this.dialog = dialog;
    }

    @Nullable
    @Opinion
    public String getPets() {
        return pets;
    }

    public void setPets(@Opinion String pets) {
        this.pets = pets;
    }

    @Nullable
    @Opinion
    public String getMusic() {
        return music;
    }

    public void setMusic(@Opinion String music) {
        this.music = music;
    }

    public boolean isMe() {
        return equals(Me.getInstance());
    }

    @Grade
    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public boolean getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public boolean getEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Integer getRidesOffered() {
        return ridesOffered;
    }

    public void setRidesOffered(Integer ridesOffered) {
        this.ridesOffered = ridesOffered;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Date getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(Date memberSince) {
        this.memberSince = memberSince;
    }

    public Integer getResponseRate() {
        return responseRate;
    }

    public void setResponseRate(Integer responseRate) {
        this.responseRate = responseRate;
    }

    public List<Review> getUserReviews() {
        return userReviews;
    }

    public void setUserReviews(List<Review> userReviews) {
        this.userReviews = userReviews;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Integer getBirthyear() {
        return birthyear;
    }

    public void setBirthyear(Integer birthyear) {
        this.birthyear = birthyear;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean getSubscribeNewsletter() {
        return subscribeNewsletter;
    }

    public void setSubscribeNewsletter(boolean subscribeNewsletter) {
        this.subscribeNewsletter = subscribeNewsletter;
    }

    public boolean isRatingAuthorized() {
        return ratingAuthorized != null && ratingAuthorized;
    }

    public void setRatingAuthorized(boolean ratingAuthorized) {
        this.ratingAuthorized = ratingAuthorized;
    }

    public Social getSocial() {
        return social;
    }

    public void setSocial(Social social) {
        this.social = social;
    }

    @IdChecked
    @NonNull
    public String getIdChecked() {
        return !TextUtils.isEmpty(idChecked) ? idChecked : NOT_APPLICABLE;
    }

    public void setIdChecked(@IdChecked String idChecked) {
        this.idChecked = idChecked;
    }

    @NonNull
    @Moderation.Status
    public String getPictureModerationStatus() {
        return null != pictureModerationStatus ? pictureModerationStatus : STATUS_UNKNOWN;
    }

    public void setPictureModerationStatus(@Nullable @Moderation.Status String pictureModerationStatus) {
        this.pictureModerationStatus = pictureModerationStatus;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    @NonNull
    @Moderation.Status
    public String getBioModerationStatus() {
        return bioModerationStatus != null ? bioModerationStatus : STATUS_UNKNOWN;
    }

    public void setBioModerationStatus(@Nullable @Moderation.Status String bioModerationStatus) {
        this.bioModerationStatus = bioModerationStatus;
    }

    public Integer getFbFriends() {
        return fbFriends;
    }

    public void setFbFriends(Integer fbFriends) {
        this.fbFriends = fbFriends;
    }

    public boolean isDisplayNameLong() {
        return displayNameLong;
    }

    public void setDisplayNameLong(boolean displayNameLong) {
        this.displayNameLong = displayNameLong;
    }

    public boolean getPhoneHidden() {
        return phoneHidden;
    }

    public void setPhoneHidden(boolean phoneHidden) {
        this.phoneHidden = phoneHidden;
    }

    public OAuth2Information getOAuth2Information() {
        return oAuth2Information;
    }

    public void setOAuth2Information(OAuth2Information oAuth2Information) {
        this.oAuth2Information = oAuth2Information;
    }

    @NonNull
    public Trip.BookingType getBookingType() {
        return bookingType != null ? bookingType : Trip.BookingType.UNKNOWN;
    }

    public void setBookingType(@Nullable Trip.BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public boolean isFetched() {
        return encryptedId != null;
    }

    public boolean isSignedCharter() {
        return signedCharter;
    }

    public void setSignedCharter(boolean signedCharter) {
        this.signedCharter = signedCharter;
    }

    public boolean isRecurringBookingEligible() {
        return recurringBookingEligible;
    }

    @NonNull
    public String getInitials() {
        if (null == displayName) {
            return "";
        }

        String[] words = displayName.split(" ");

        if (words.length > 0) {
            final StringBuilder result = new StringBuilder(words.length);
            for (String word : words) {
                if (word.length() > 0) {
                    result.append(Character.toUpperCase(word.charAt(0)));
                }
            }

            return result.toString();
        } else {
            return String.valueOf(displayName.charAt(0));
        }
    }

    public boolean equals(User user) {
        return null != encryptedId
            && null != user
            && null != user.getEncryptedId()
            && encryptedId.equals(user.getEncryptedId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(this.encryptedId);
        dest.writeString(this.picture);
        dest.writeByte(this.hasPicture ? (byte) 1 : (byte) 0);
        dest.writeInt(this.age);
        dest.writeFloat(this.rating);
        dest.writeInt(this.ratingCount);
        dest.writeString(this.smoking);
        dest.writeString(this.dialog);
        dest.writeString(this.pets);
        dest.writeString(this.music);
        dest.writeInt(this.grade);
        dest.writeInt(this.gender == null ? -1 : this.gender.ordinal());
        dest.writeInt(this.phoneVerified ? 1 : 0);
        dest.writeInt(this.emailVerified ? 1 : 0);
        dest.writeInt(this.phoneHidden ? 1 : 0);
        dest.writeValue(this.ridesOffered);
        dest.writeLong(this.lastLogin != null ? lastLogin.getTime() : -1);
        dest.writeLong(this.memberSince != null ? memberSince.getTime() : -1);
        dest.writeValue(this.responseRate);
        dest.writeTypedList(this.userReviews);
        dest.writeParcelable(this.phone, 0);
        dest.writeString(this.firstname);
        dest.writeString(this.lastname);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeValue(this.birthyear);
        dest.writeInt(this.subscribeNewsletter ? 1 : 0);
        dest.writeString(this.pictureModerationStatus);
        dest.writeString(this.bio);
        dest.writeString(this.bioModerationStatus);
        dest.writeValue(this.fbFriends);
        dest.writeInt(this.displayNameLong ? 1 : 0);
        dest.writeString(this.idUser);
        dest.writeValue(this.ratingAuthorized);
        dest.writeParcelable(this.oAuth2Information, 0);
        dest.writeString(this.displayName);
        dest.writeInt(getBookingType().ordinal());
        dest.writeParcelable(this.social, 0);
        dest.writeInt(this.signedCharter ? 1 : 0);
        dest.writeString(idChecked);
    }

    public enum Gender {
        @SerializedName("_UE_M")M,
        @SerializedName("_UE_MISS")MISS,
        @SerializedName("_UE_MRS")MRS
    }

    public enum UserType {
        DRIVER,
        PASSENGER,
        UNKNOWN
    }

    @Retention(RetentionPolicy.SOURCE) @IntDef({
        GRADE_UNKNOWN, GRADE_BEGINNER, GRADE_REGULAR, GRADE_CONFIRMED, GRADE_EXPERT,
        GRADE_AMBASSADOR
    })

    public @interface Grade {
    }

    @StringDef({
        TO_CHECK, CHECKED, PENDING, NOT_APPLICABLE
    }) public @interface IdChecked {
    }

    @Retention(RetentionPolicy.SOURCE) @StringDef({
        OPINION_YES, OPINION_NO, OPINION_MAYBE,
    })

    public @interface Opinion {
    }
}