package com.codingnight.blablavoiceapp;

import android.support.annotation.StringDef;

public interface Moderation {
    @StringDef({
        STATUS_PENDING, STATUS_VALIDATED, STATUS_REFUSED, STATUS_UNKNOWN
    }) @interface Status {
    }

    /**
     * PENDING status return by the API mean this photo is in moderation spooler.
     */
    String STATUS_PENDING = "PENDING";
    /**
     * MODERATED status return by the API mean refused automatically or manually.
     */
    String STATUS_REFUSED = "MODERATED";

    /**
     * ACTIVE status return by the API mean this photo has been accepted automatically or manually.
     */
    String STATUS_VALIDATED = "ACTIVE";

    /**
     * Fallback status in case we don't know yet this status.
     */
    String STATUS_UNKNOWN = "UNKNOWN";
}
