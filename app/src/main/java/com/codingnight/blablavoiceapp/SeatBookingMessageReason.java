package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class SeatBookingMessageReason implements Parcelable {

    public static final Creator<SeatBookingMessageReason> CREATOR =
        new Creator<SeatBookingMessageReason>() {
            public SeatBookingMessageReason createFromParcel(Parcel source) {
                return new SeatBookingMessageReason(source);
            }

            public SeatBookingMessageReason[] newArray(int size) {
                return new SeatBookingMessageReason[size];
            }
        };
    private String id;
    private String label;

    public SeatBookingMessageReason() {
    }

    private SeatBookingMessageReason(Parcel in) {
        this.id = in.readString();
        this.label = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return label;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.label);
    }
}
