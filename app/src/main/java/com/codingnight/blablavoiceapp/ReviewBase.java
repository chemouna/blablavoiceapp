package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import java.io.Serializable;

public abstract class ReviewBase implements Parcelable, Serializable {

    private String comment;
    private int globalRating;
    private RatingCount ratingCount;

    protected ReviewBase() {
        this.comment = null;
        this.globalRating = RatingCount.VALUE_UNKNOWN;
        this.ratingCount = new RatingCount(globalRating, -1);
    }

    protected ReviewBase(@NonNull String comment, int globalRating,
        @NonNull RatingCount ratingCount) {
        this.comment = comment;
        this.globalRating = globalRating;
        this.ratingCount = ratingCount;
    }

    protected ReviewBase(Parcel in) {
        this.comment = in.readString();
        this.globalRating = in.readInt();
        this.ratingCount = in.readParcelable(RatingCount.class.getClassLoader());
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getGlobalRating() {
        return globalRating;
    }

    public void setGlobalRating(int globalRating) {
        this.globalRating = globalRating;

        if (ratingCount != null) {
            ratingCount = new RatingCount(globalRating, ratingCount.getCount());
        }
    }

    public RatingCount getRatingCount() {
        if (ratingCount == null) {
            ratingCount = new RatingCount(globalRating, -1);
        }

        return ratingCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.comment);
        dest.writeInt(this.globalRating);
        dest.writeParcelable(this.ratingCount, 0);
    }
}

