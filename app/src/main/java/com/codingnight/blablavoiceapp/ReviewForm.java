package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.StringDef;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class ReviewForm extends ReviewBase implements Parcelable, Serializable {
    public static final String ROLE_DRIVER = "driver";
    public static final String ROLE_PASSENGER = "passenger";
    public static final String ROLE_OTHER = "other";
    public static final int DRIVING_SKILL_UNKNOWN = -1;
    public static final int DRIVING_SKILL_OPTIONAL = 0;
    public static final int DRIVING_SKILL_TO_AVOID = 1;
    public static final int DRIVING_SKILL_TO_BE_IMPROVED = 2;
    public static final int DRIVING_SKILL_PLEASANT = 3;
    public static final Creator<ReviewForm> CREATOR = new Creator<ReviewForm>() {
        public ReviewForm createFromParcel(Parcel source) {
            return new ReviewForm(source);
        }

        public ReviewForm[] newArray(int size) {
            return new ReviewForm[size];
        }
    };
    @Role private String role;
    @DrivingSkill private Integer drivingRating;
    private Integer drivingRatingOptional;
    public ReviewForm() {
        super();

        this.role = null;
        this.drivingRating = null;
        this.drivingRatingOptional = null;
    }

    public ReviewForm(@NonNull String comment, int globalRating, @NonNull RatingCount ratingCount,
        @NonNull @Role String role, int drivingRating, int drivingRatingOptional) {
        super(comment, globalRating, ratingCount);

        this.role = role;
        this.drivingRating = drivingRating;
        this.drivingRatingOptional = drivingRatingOptional;
    }

    @SuppressWarnings("ResourceType")
    private ReviewForm(Parcel in) {
        super(in);

        this.role = in.readString();
        this.drivingRating = (Integer) in.readValue(Integer.class.getClassLoader());
        this.drivingRatingOptional = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    @Role
    public String getRole() {
        return role;
    }

    public void setRole(@Role String role) {
        this.role = role;
    }

    @DrivingSkill
    public int getDrivingSkill() {
        if (drivingRatingOptional != null) {
            return DRIVING_SKILL_OPTIONAL;
        } else if (drivingRating != null) {
            return drivingRating;
        } else {
            return DRIVING_SKILL_UNKNOWN;
        }
    }

    public void setDrivingSkill(@DrivingSkill int drivingSkill) {
        if (drivingSkill == DRIVING_SKILL_OPTIONAL) {
            this.drivingRating = null;
            this.drivingRatingOptional = 1;
        } else {
            this.drivingRating = drivingSkill;
            this.drivingRatingOptional = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);

        dest.writeString(role);
        dest.writeValue(drivingRating);
        dest.writeValue(drivingRatingOptional);
    }

    @Retention(RetentionPolicy.SOURCE) @StringDef({
        ROLE_DRIVER, ROLE_PASSENGER, ROLE_OTHER
    }) public @interface Role {
    }

    @Retention(RetentionPolicy.SOURCE) @IntDef({
        DRIVING_SKILL_UNKNOWN, DRIVING_SKILL_OPTIONAL, DRIVING_SKILL_TO_AVOID,
        DRIVING_SKILL_TO_BE_IMPROVED, DRIVING_SKILL_PLEASANT
    }) public @interface DrivingSkill {
    }
}
