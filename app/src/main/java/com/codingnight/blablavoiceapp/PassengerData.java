package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class PassengerData implements Parcelable {

    public static final Creator<PassengerData> CREATOR = new Creator<PassengerData>() {
        public PassengerData createFromParcel(Parcel source) {
            return new PassengerData(source);
        }

        public PassengerData[] newArray(int size) {
            return new PassengerData[size];
        }
    };
    private String name;
    private int age;
    private String countryCode;
    private String rawInput;

    public PassengerData(String name, int age, String countryCode, String rawInput) {
        this.name = name;
        this.age = age;
        this.countryCode = countryCode;
        this.rawInput = rawInput;
    }

    private PassengerData(Parcel in) {
        this.name = in.readString();
        this.age = in.readInt();
        this.countryCode = in.readString();
        this.rawInput = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getRawInput() {
        return rawInput;
    }

    public void setRawInput(String rawInput) {
        this.rawInput = rawInput;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.age);
        dest.writeString(this.countryCode);
        dest.writeString(this.rawInput);
    }
}
