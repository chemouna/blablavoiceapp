package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seat implements Parcelable {

    public static final Creator<Seat> CREATOR = new Creator<Seat>() {
        public Seat createFromParcel(Parcel source) {
            return new Seat(source);
        }

        public Seat[] newArray(int size) {
            return new Seat[size];
        }
    };
    private String encryptedId;
    private int nbSeats;
    private Trip trip;
    private Price unitPrice;
    private Price passengerRefund;
    private Price driverCompensation;
    private Price pricePaid;
    private Price halfCommission;
    private Price commission;
    private Price shouldPaidCommission;
    private Price pricePaidWithoutCommission;
    private Price priceWithoutCommission;
    private Price halfPricePaid;
    private String code;
    private String bookingStatus;
    private boolean tripIsPassed;
    private String expireDate;
    private User driver;
    private String ticketId;
    private ExpirationRange expirationRange;
    @SerializedName("payment_solutions") private List<PaymentSolution> paymentSolutions;
    private String paymentReference;
    private String bookingCode;
    private String offerComment;
    private int creditNumberRequired;
    private int creditNumberAvailable;
    private int hasEnoughCredit;
    private List<Date> seatExpireRange;
    private Date defaultSeatExpire;
    private int selectedProviderPayment;
    private String merchantReference;
    private PassengerData passengerData;
    private boolean canBookForSomeoneElse;

    public Seat() {
    }

    private Seat(Parcel in) {
        this.encryptedId = in.readString();
        this.nbSeats = in.readInt();
        this.trip = in.readParcelable(Trip.class.getClassLoader());
        this.unitPrice = in.readParcelable(Price.class.getClassLoader());
        this.passengerRefund = in.readParcelable(Price.class.getClassLoader());
        this.driverCompensation = in.readParcelable(Price.class.getClassLoader());
        this.pricePaid = in.readParcelable(Price.class.getClassLoader());
        this.halfCommission = in.readParcelable(Price.class.getClassLoader());
        this.commission = in.readParcelable(Price.class.getClassLoader());
        this.pricePaidWithoutCommission = in.readParcelable(Price.class.getClassLoader());
        this.halfPricePaid = in.readParcelable(Price.class.getClassLoader());
        this.code = in.readString();
        this.bookingStatus = in.readString();
        this.tripIsPassed = in.readByte() != 0;
        this.expireDate = in.readString();
        this.driver = in.readParcelable(User.class.getClassLoader());
        this.ticketId = in.readString();
        this.expirationRange = in.readParcelable(ExpirationRange.class.getClassLoader());
        paymentSolutions = new ArrayList<PaymentSolution>();
        in.readTypedList(paymentSolutions, PaymentSolution.CREATOR);
        this.paymentReference = in.readString();
        this.bookingCode = in.readString();
        this.offerComment = in.readString();
        this.priceWithoutCommission = in.readParcelable(Price.class.getClassLoader());
        this.creditNumberAvailable = in.readInt();
        this.creditNumberRequired = in.readInt();
        this.hasEnoughCredit = in.readInt();
        seatExpireRange = new ArrayList<Date>();
        in.readList(seatExpireRange, null);
        Long tmplong = in.readLong();
        if (tmplong > 0) {
            this.defaultSeatExpire = new Date(tmplong);
        }
        this.selectedProviderPayment = in.readInt();
        this.merchantReference = in.readString();
        this.shouldPaidCommission = in.readParcelable(Price.class.getClassLoader());
        this.passengerData = in.readParcelable(PassengerData.class.getClassLoader());
        this.canBookForSomeoneElse = in.readByte() != 0;
    }

    public List<Date> getSeatExpireRange() {
        return seatExpireRange;
    }

    public void setSeatExpireRange(List<Date> seatExpireRange) {
        this.seatExpireRange = seatExpireRange;
    }

    public int getCreditNumberRequired() {
        return creditNumberRequired;
    }

    public void setCreditNumberRequired(int creditNumberRequired) {
        this.creditNumberRequired = creditNumberRequired;
    }

    public int getCreditNumberAvailable() {
        return creditNumberAvailable;
    }

    public void setCreditNumberAvailable(int creditNumberAvailable) {
        this.creditNumberAvailable = creditNumberAvailable;
    }

    public int getHasEnoughCredit() {
        return hasEnoughCredit;
    }

    public void setHasEnoughCredit(int hasEnoughCredit) {
        this.hasEnoughCredit = hasEnoughCredit;
    }

    public ExpirationRange getExpirationRange() {
        return expirationRange;
    }

    public void setExpirationRange(ExpirationRange expirationRange) {
        this.expirationRange = expirationRange;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public String getEncryptedId() {
        return encryptedId;
    }

    public void setEncryptedId(String encryptedId) {
        this.encryptedId = encryptedId;
    }

    public int getNbSeats() {
        return nbSeats;
    }

    public void setNbSeats(int nbSeats) {
        this.nbSeats = nbSeats;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Price getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Price unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Price getPassengerRefund() {
        return passengerRefund;
    }

    public void setPassengerRefund(Price passengerRefund) {
        this.passengerRefund = passengerRefund;
    }

    public Price getDriverCompensation() {
        return driverCompensation;
    }

    public void setDriverCompensation(Price driverCompensation) {
        this.driverCompensation = driverCompensation;
    }

    public Price getPricePaid() {
        return pricePaid;
    }

    public void setPricePaid(Price pricePaid) {
        this.pricePaid = pricePaid;
    }

    public Price getHalfCommission() {
        return halfCommission;
    }

    public void setHalfCommission(Price halfCommission) {
        this.halfCommission = halfCommission;
    }

    public Price getCommission() {
        return commission;
    }

    public void setCommission(Price commission) {
        this.commission = commission;
    }

    public Price getShouldPaidCommission() {
        return shouldPaidCommission;
    }

    public void setShouldPaidCommission(Price shouldPaidCommission) {
        this.shouldPaidCommission = shouldPaidCommission;
    }

    public Price getPricePaidWithoutCommission() {
        return pricePaidWithoutCommission;
    }

    public void setPricePaidWithoutCommission(Price pricePaidWithoutCommission) {
        this.pricePaidWithoutCommission = pricePaidWithoutCommission;
    }

    public Price getHalfPricePaid() {
        return halfPricePaid;
    }

    public void setHalfPricePaid(Price halfPricePaid) {
        this.halfPricePaid = halfPricePaid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public boolean isTripIsPassed() {
        return tripIsPassed;
    }

    public void setTripIsPassed(boolean tripIsPassed) {
        this.tripIsPassed = tripIsPassed;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public List<PaymentSolution> getPaymentSolutions() {
        return paymentSolutions;
    }

    public void setPaymentSolutions(List<PaymentSolution> paymentSolutions) {
        this.paymentSolutions = paymentSolutions;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public String getBookingCode() {
        return bookingCode;
    }

    public void setBookingCode(String bookingCode) {
        this.bookingCode = bookingCode;
    }

    public String getOfferComment() {
        return offerComment;
    }

    public void setOfferComment(String offerComment) {
        this.offerComment = offerComment;
    }

    public Price getPriceWithoutCommission() {
        return priceWithoutCommission;
    }

    public void setPriceWithoutCommission(Price priceWithoutCommission) {
        this.priceWithoutCommission = priceWithoutCommission;
    }

    public Date getDefaultSeatExpire() {
        return defaultSeatExpire;
    }

    public void setDefaultSeatExpire(Date defaultSeatExpire) {
        this.defaultSeatExpire = defaultSeatExpire;
    }

    public int getSelectedProviderPayment() {
        return selectedProviderPayment;
    }

    public void setSelectedProviderPayment(int selectedProviderPayment) {
        this.selectedProviderPayment = selectedProviderPayment;
    }

    public String getMerchantReference() {
        return merchantReference;
    }

    public void setMerchantReference(String merchantReference) {
        this.merchantReference = merchantReference;
    }

    public PassengerData getPassengerData() {
        return passengerData;
    }

    public void setPassengerData(PassengerData passengerData) {
        this.passengerData = passengerData;
    }

    public boolean isCanBookForSomeoneElse() {
        return canBookForSomeoneElse;
    }

    public void setCanBookForSomeoneElse(boolean canBookForSomeoneElse) {
        this.canBookForSomeoneElse = canBookForSomeoneElse;
    }

    public int getNoPaymentSolutionIfAvailable() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isNoPayment()) {
                return solution.getId();
            }
        }

        return -1;
    }

    public boolean matchNoPaymentSolution() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isNoPayment()) {
                return true;
            }
        }

        return false;
    }

    public boolean matchPaymentSolution() {
        if (matchDevPaymentSolution()
            || matchAdyenPaymentSolution()
            || matchPaypalPaymentSolution()
            || matchPaylinePaymentSolution()) {
            return true;
        }
        return false;
    }

    public boolean matchDevPaymentSolution() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isSimpleSimple()) {
                return true;
            }
        }

        return false;
    }

    public boolean matchAdyenPaymentSolution() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isAdyen()) {
                return true;
            }
        }

        return false;
    }

    public boolean matchPaypalPaymentSolution() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isPayPal()) {
                return true;
            }
        }

        return false;
    }

    public boolean matchPaylinePaymentSolution() {
        for (PaymentSolution solution : paymentSolutions) {
            if (solution.isPayline()) {
                return true;
            }
        }

        return false;
    }

    public boolean isNoFee() {
        return commission != null && commission.getValue() == 0f;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.encryptedId);
        dest.writeInt(this.nbSeats);
        dest.writeParcelable(this.trip, 0);
        dest.writeParcelable(this.unitPrice, 0);
        dest.writeParcelable(this.passengerRefund, 0);
        dest.writeParcelable(this.driverCompensation, 0);
        dest.writeParcelable(this.pricePaid, 0);
        dest.writeParcelable(this.halfCommission, 0);
        dest.writeParcelable(this.commission, 0);
        dest.writeParcelable(this.pricePaidWithoutCommission, 0);
        dest.writeParcelable(this.halfPricePaid, 0);
        dest.writeString(this.code);
        dest.writeString(this.bookingStatus);
        dest.writeByte(tripIsPassed ? (byte) 1 : (byte) 0);
        dest.writeString(this.expireDate);
        dest.writeParcelable(this.driver, 0);
        dest.writeString(this.ticketId);
        dest.writeParcelable(this.expirationRange, 0);
        dest.writeTypedList(paymentSolutions);
        dest.writeString(paymentReference);
        dest.writeString(this.bookingCode);
        dest.writeString(this.offerComment);
        dest.writeParcelable(this.priceWithoutCommission, 0);
        dest.writeInt(this.creditNumberAvailable);
        dest.writeInt(this.creditNumberRequired);
        dest.writeInt(this.hasEnoughCredit);
        dest.writeList(this.seatExpireRange);
        if (null != this.defaultSeatExpire) {
            dest.writeLong(this.defaultSeatExpire.getTime());
        } else {
            dest.writeLong(0);
        }
        dest.writeInt(this.selectedProviderPayment);
        dest.writeString(this.merchantReference);
        dest.writeParcelable(this.shouldPaidCommission, 0);
        dest.writeParcelable(this.passengerData, 0);
        dest.writeByte(canBookForSomeoneElse ? (byte) 1 : (byte) 0);
    }
}
