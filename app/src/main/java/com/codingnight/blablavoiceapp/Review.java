package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public final class Review extends ReviewBase implements Parcelable, Serializable {
    public static final Creator<Review> CREATOR = new Creator<Review>() {
        public Review createFromParcel(Parcel source) {
            return new Review(source);
        }

        public Review[] newArray(int size) {
            return new Review[size];
        }
    };
    @Nullable private final String encryptedId;
    private final Date publicationDate;
    private final String senderDisplayName;
    @SerializedName("sender_profil_picture") private final String senderProfilePicture;
    private final String senderId;
    @Nullable private final String receiverId;
    @Nullable private final String receiverDisplayName;
    @Nullable @SerializedName("receiver_profil_picture") private final String
        receiverProfilePicture;
    @Nullable private ReviewResponse.Wrapper responses;
    private boolean leaveRating;
    private boolean responseAuthorized;

    public Review(@NonNull String comment, int globalRating, @NonNull RatingCount ratingCount,
        @NonNull Date publicationDate, @NonNull String senderDisplayName,
        @NonNull String senderProfilePicture, @NonNull String senderId, boolean leaveRating,
        boolean responseAuthorized) {
        this(comment, globalRating, ratingCount, publicationDate, senderDisplayName,
            senderProfilePicture, senderId, leaveRating, responseAuthorized, null, null, null, null,
            null);
    }

    public Review(@NonNull String comment, int globalRating, @NonNull RatingCount ratingCount,
        @NonNull Date publicationDate, @NonNull String senderDisplayName,
        @NonNull String senderProfilePicture, @NonNull String senderId, boolean leaveRating,
        boolean responseAuthorized, @Nullable String receiverId,
        @Nullable String receiverDisplayName, @Nullable String receiverProfilePicture,
        @Nullable String encryptedId, @Nullable ReviewResponse.Wrapper responses) {
        super(comment, globalRating, ratingCount);

        this.receiverId = receiverId;
        this.receiverDisplayName = receiverDisplayName;
        this.receiverProfilePicture = receiverProfilePicture;
        this.leaveRating = leaveRating;
        this.responseAuthorized = responseAuthorized;
        this.encryptedId = encryptedId;
        this.publicationDate = publicationDate;
        this.senderDisplayName = senderDisplayName;
        this.senderProfilePicture = senderProfilePicture;
        this.senderId = senderId;
        this.responses = responses;
    }

    public Review(ReviewForm reviewForm) {
        super(reviewForm.getComment(), reviewForm.getRatingCount().getValue(),
            new RatingCount(reviewForm.getGlobalRating(), -1));

        this.encryptedId = null;
        this.publicationDate = Calendar.getInstance().getTime();
        this.senderDisplayName = Me.getInstance().getDisplayName();
        this.senderProfilePicture = Me.getInstance().getPicture();
        this.senderId = Me.getInstance().getEncryptedId();
        this.responses = null;
        this.receiverId = null;
        this.receiverDisplayName = null;
        this.receiverProfilePicture = null;
    }

    private Review(Parcel in) {
        super(in);
        this.encryptedId = in.readString();
        long tmpPublicationDate = in.readLong();
        this.publicationDate = tmpPublicationDate == -1 ? null : new Date(tmpPublicationDate);
        this.senderDisplayName = in.readString();
        this.senderProfilePicture = in.readString();
        this.senderId = in.readString();
        this.responses = in.readParcelable(ReviewResponse.Wrapper.class.getClassLoader());
        this.leaveRating = in.readByte() != 0;
        this.responseAuthorized = in.readByte() != 0;
        this.receiverId = in.readString();
        this.receiverDisplayName = in.readString();
        this.receiverProfilePicture = in.readString();
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public String getSenderDisplayName() {
        return senderDisplayName;
    }

    @Nullable
    public String getEncryptedId() {
        return encryptedId;
    }

    @Nullable
    public String getFirstResponse() {
        return responses != null && responses.getResponses().size() > 0 ? responses.getResponses()
            .get(0)
            .getResponse() : null;
    }

    public void setFirstResponse(String response) {
        if (responses == null) {
            responses = new ReviewResponse.Wrapper();
        }

        responses.getResponses().add(0, new ReviewResponse(response));
    }

    public boolean isLeaveRating() {
        return leaveRating;
    }

    public void setLeaveRating(boolean leaveRating) {
        this.leaveRating = leaveRating;
    }

    public boolean isResponseAuthorized() {
        return responseAuthorized;
    }

    public void setResponseAuthorized(boolean responseAuthorized) {
        this.responseAuthorized = responseAuthorized;
    }

    @Nullable
    public ReviewUser getSender() {
        if (senderId != null && senderDisplayName != null) {
            return new ReviewUser(senderId, senderDisplayName, senderProfilePicture);
        }

        return null;
    }

    @Nullable
    public ReviewUser getReceiver() {
        if (receiverId != null && receiverDisplayName != null) {
            return new ReviewUser(receiverId, receiverDisplayName, receiverProfilePicture);
        }

        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.encryptedId);
        dest.writeLong(publicationDate != null ? publicationDate.getTime() : -1);
        dest.writeString(this.senderDisplayName);
        dest.writeString(this.senderProfilePicture);
        dest.writeString(this.senderId);
        dest.writeParcelable(this.responses, 0);
        dest.writeByte(leaveRating ? (byte) 1 : (byte) 0);
        dest.writeByte(responseAuthorized ? (byte) 1 : (byte) 0);
        dest.writeString(this.senderId);
        dest.writeString(this.senderDisplayName);
        dest.writeString(this.senderProfilePicture);
    }
}

