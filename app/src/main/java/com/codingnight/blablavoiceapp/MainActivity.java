package com.codingnight.blablavoiceapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.actions.SearchIntents;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.speech.RecognizerIntent.EXTRA_CONFIDENCE_SCORES;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    protected static final int SPEECH_RECOGNITION_REQUEST = 1;

    @BindView(R.id.fab) FloatingActionButton fab;
    private EditText resultText;

    private RecyclerView tripsRecyclerView;
    private RecyclerView.Adapter tripsAdapter;
    private ArrayList<Trip> trips;
    private LinearLayout emptyState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        emptyState = (LinearLayout) findViewById(R.id.empty_state);
        resultText = (EditText)findViewById(R.id.result_text);
        resultText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Log.d(TAG, "onEditorAction: " + actionId);
                if ((actionId & EditorInfo.IME_MASK_ACTION) == EditorInfo.IME_ACTION_SEARCH) {
                    sendRequest(v.getText().toString());
                    return true;
                }
                return false;
            }
        });

        trips = new ArrayList<>();
        tripsRecyclerView = (RecyclerView)findViewById(R.id.trips_recycler_view);
        tripsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        tripsRecyclerView.setHasFixedSize(true);
        tripsAdapter = new TripsAdapter(getApplicationContext(), trips);
        tripsRecyclerView.setAdapter(tripsAdapter);

        ButterKnife.bind(this);
        ActivityCompat.requestPermissions(this, new String[] { Manifest.permission.RECORD_AUDIO }, 1);

        Intent intent = getIntent();
        if (intent != null && SearchIntents.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchIntents.EXTRA_QUERY);
            Log.d(TAG, "onCreate: " + query);
            resultText.setText(query);
        }
    }

    @OnClick(R.id.fab)
    public void startRecording() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_PREFERENCE, "en-US");
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, this.getPackageName());
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
        try {
            startActivityForResult(intent, SPEECH_RECOGNITION_REQUEST);
        } catch (Exception e) {
            Toast.makeText(this, "Error initializing speech to text engine.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SPEECH_RECOGNITION_REQUEST && resultCode == RESULT_OK) {
            float[] confidenceScores = data.getFloatArrayExtra(EXTRA_CONFIDENCE_SCORES);
            ArrayList<String> thingsYouSaid = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            resultText.setText(thingsYouSaid.get(0));

            if (confidenceScores[0] < 0.8)
                Toast.makeText(this, "Please repeat your request", Toast.LENGTH_LONG).show();
            else
                sendRequest(resultText.getText().toString());

            for (int i = 0; i < confidenceScores.length; i++)
                Log.d(TAG, "onActivityResult: " + thingsYouSaid.get(i) + " score = " + confidenceScores[i]);
        }
    }

    public void sendRequest(String message) {
        Call<FindRequestResults> call = BlaBlaVoiceApiService.getApi().trips(message);
        call.enqueue(new Callback<FindRequestResults>() {
            @Override
            public void onResponse(Call<FindRequestResults> call, Response<FindRequestResults> response) {
                if (response.body() != null) {
                    List<Trip> responseTrips = response.body().data.getTrips();

                    if (responseTrips.size() > 0) {
                        tripsRecyclerView.setVisibility(View.VISIBLE);
                        emptyState.setVisibility(View.GONE);
                        trips.clear();
                        trips.addAll(responseTrips);
                        Log.d(TAG, "onResponse: " + trips.toString());
                        tripsAdapter.notifyDataSetChanged();
                    }
                    else {
                        showEmptyStateAndResetResults();
                    }
                    hideKeyboard();
                }
            }

            @Override
            public void onFailure(Call<FindRequestResults> call, Throwable t) {
                if (null != t.getLocalizedMessage()) {
                    Log.e(TAG, t.getLocalizedMessage());
                }
                showEmptyStateAndResetResults();
            }
        });
    }

    private void showEmptyStateAndResetResults() {
        trips.clear();
        tripsAdapter.notifyDataSetChanged();
        tripsRecyclerView.setVisibility(View.GONE);
        emptyState.setVisibility(View.VISIBLE);
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
