package com.codingnight.blablavoiceapp;

import android.content.Context;
import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Trip extends TripBase implements Serializable{
    // remember to update BlablacarApi.priceSuggestLevel() params if you change that
    public static final int MAX_STOPOVER_COUNT = 6;
    public static final int DEFAULT_SEATS_COUNT = 3;
    public static final int MIN_PRICE = 1;
    public static final Creator<Trip> CREATOR = new Creator<Trip>() {
        public Trip createFromParcel(Parcel source) {
            return new Trip(source);
        }

        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };
    private Price price;
    private User user;
    @Nullable private Integer seatsLeft;
    private List<StopOver> stopOvers;
    private Measure distance;
    private Measure duration;
    private Detour detour;
    private Schedule schedule;
    private String comment;
    private LuggageSize luggage;
    private Car car;
    private boolean viaggioRosa;
    private boolean freeway;
    private Date lastVisitDate;
    private boolean contacted;
    private ModeList bookingMode;
    @Nullable private BookingType bookingType;
    private int answerDelay;
    private int questionResponseCount;
    private int viewCount;
    private boolean crossBorderAlert;
    private SeatBooking userSeat;
    private List<String> locationsToDisplay;
    private boolean isComfort;

    @Retention(RetentionPolicy.SOURCE) @StringDef({
        UNAVAILABLE, PUBLIC, PRIVATE
    }) public @interface MessagingStatus {
    }

    public static final String UNAVAILABLE = "unavailable";
    public static final String PUBLIC = "public";
    public static final String PRIVATE = "private";

    @SerializedName("messaging_status") @MessagingStatus private String messagingStatus;

    @Nullable private String masterTripOfferEncryptedId;

    public Trip() {

    }

    public String getFormattedRouteWithSubtrips(Context context) {
        String departure = Place.getCityName(departurePlace);
        String arrival = Place.getCityName(arrivalPlace);

        if (null == departure || null == arrival) {
            return null;
        }

        String separator = context.getString(R.string.global_route_separator);
        String ext = context.getString(R.string.global_route_ext_route);

        String formattedRoute = "";

        if (null != locationsToDisplay && locationsToDisplay.size() > 0 && !departure.equals(
            locationsToDisplay.get(0))) {
            formattedRoute = ext + separator + formattedRoute;
        }

        formattedRoute += departure + separator;

        if (null != locationsToDisplay) {
            int departureIndex = locationsToDisplay.indexOf(departure);
            int arrivalIndex = locationsToDisplay.indexOf(arrival);

            if (arrivalIndex - departureIndex > 1) {
                formattedRoute += ext + separator;
            }
        }

        formattedRoute += arrival;

        if (null != locationsToDisplay && locationsToDisplay.size() > 0 && !arrival.equals(
            locationsToDisplay.get(locationsToDisplay.size() - 1))) {
            formattedRoute += separator + ext;
        }

        return formattedRoute;
    }

    public List<String> getLocationsToDisplay() {
        return locationsToDisplay;
    }

    public void setLocationsToDisplay(List<String> locationsToDisplay) {
        this.locationsToDisplay = locationsToDisplay;
    }

    public SeatBooking getUserSeat() {
        return userSeat;
    }

    public void setUserSeat(SeatBooking userSeat) {
        this.userSeat = userSeat;
    }

    @NonNull
    public BookingType getBookingType() {
        return bookingType != null ? bookingType : BookingType.UNKNOWN;
    }

    public void setBookingType(@Nullable BookingType bookingType) {
        this.bookingType = bookingType;
    }

    public int getQuestionResponseCount() {
        return questionResponseCount;
    }

    public void setQuestionResponseCount(int questionResponseCount) {
        this.questionResponseCount = questionResponseCount;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Nullable
    public Integer getSeatsLeft() {
        return seatsLeft;
    }

    public void setSeatsLeft(@Nullable Integer seatsLeft) {
        this.seatsLeft = seatsLeft;
    }

    public List<StopOver> getStopOvers() {
        return stopOvers;
    }

    public void setStopOvers(List<StopOver> stopOvers) {
        this.stopOvers = stopOvers;
    }

    public Measure getDistance() {
        return distance;
    }

    public void setDistance(Measure distance) {
        this.distance = distance;
    }

    public Measure getDuration() {
        return duration;
    }

    public void setDuration(Measure duration) {
        this.duration = duration;
    }

    public Detour getDetour() {
        return detour;
    }

    public void setDetour(Detour detour) {
        this.detour = detour;
    }

    public Schedule getSchedule() {
        return schedule;
    }

    public void setSchedule(Schedule schedule) {
        this.schedule = schedule;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LuggageSize getLuggage() {
        return luggage;
    }

    public void setLuggage(LuggageSize luggage) {
        this.luggage = luggage;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public boolean isViaggioRosa() {
        return viaggioRosa;
    }

    public void setViaggioRosa(boolean viaggioRosa) {
        this.viaggioRosa = viaggioRosa;
    }

    public boolean isFreeway() {
        return freeway;
    }

    public void setFreeway(boolean freeway) {
        this.freeway = freeway;
    }

    public Date getLastVisitDate() {
        return lastVisitDate;
    }

    public void setLastVisitDate(Date lastVisitDate) {
        this.lastVisitDate = lastVisitDate;
    }

    public boolean isContacted() {
        return contacted;
    }

    public void setContacted(boolean contacted) {
        this.contacted = contacted;
    }

    public Date getEstimatedArrivalDate() {
        return new Date(getDepartureDate().getTime() + duration.getValue() * 1000);
    }

    public ModeList getBookingMode() {
        return bookingMode;
    }

    public void setBookingMode(ModeList bookingMode) {
        this.bookingMode = bookingMode;
    }

    public int getAnswerDelay() {
        return answerDelay;
    }

    public void setAnswerDelay(int answerDelay) {
        this.answerDelay = answerDelay;
    }

    public int getViewCount() {
        return viewCount;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public boolean isCrossBorderAlert() {
        return crossBorderAlert;
    }

    public void setCrossBorderAlert(boolean crossBorderAlert) {
        this.crossBorderAlert = crossBorderAlert;
    }

    public final boolean isFully() {
        return price != null && seatsLeft != null && userSeat != null;
    }

    public boolean isComfort() {
        return isComfort;
    }

    public void setIsComfort(boolean isComfort) {
        this.isComfort = isComfort;
    }

    @Nullable
    public String getMasterTripOfferEncryptedId() {
        return masterTripOfferEncryptedId;
    }

    public void setMasterTripOfferEncryptedId(@NonNull String masterTripOfferEncryptedId) {
        this.masterTripOfferEncryptedId = masterTripOfferEncryptedId;
    }

    public String getMessagingStatus() {
        return messagingStatus;
    }

    public void setMessagingStatus(String messagingStatus) {
        this.messagingStatus = messagingStatus;
    }

    public boolean isRecurring() {
        return !TextUtils.isEmpty(masterTripOfferEncryptedId);
    }

    /**
     * Used to show the correct route to get the real departure place of this trip
     *
     * @return the trip's departure place if not "my departure place", null otherwise
     */
    public Place getOptDeparturePlace() {

        if (stopOvers != null) {
            Place myDeparturePlace = null;
            int i = 0;

            for (StopOver stopover : stopOvers) {
                if (stopover.isDeparturePlace()) {
                    myDeparturePlace = stopover;
                    break;
                }
                i++;
            }

            if (myDeparturePlace != null && i > 0) {
                return stopOvers.get(0);
            }
        }

        return null;
    }

    /**
     * Used to show the correct route to get the real arrival place of this trip
     *
     * @return the trip's arrival place if not "my arrival place", null otherwise
     */
    public Place getOptArrivalPlace() {

        if (stopOvers != null) {
            Place myArrivalPlace = null;
            int i = 0;

            for (StopOver stopover : stopOvers) {
                if (stopover.isArrivalPlace()) {
                    myArrivalPlace = stopover;
                    break;
                }
                i++;
            }

            if (myArrivalPlace != null && i < stopOvers.size() - 1) {
                return stopOvers.get(stopOvers.size() - 1);
            }
        }

        return null;
    }

    public boolean isMine() {
        return user != null && !TextUtils.isEmpty(user.getEncryptedId()) && !TextUtils.isEmpty(
            Me.getInstance().getEncryptedId()) && user.getEncryptedId().equals(Me.getInstance().getEncryptedId());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.price, 0);
        dest.writeParcelable(this.user, 0);
        dest.writeValue(this.seatsLeft);
        dest.writeTypedList(stopOvers);
        dest.writeParcelable(this.distance, 0);
        dest.writeParcelable(this.duration, 0);
        dest.writeInt(this.detour == null ? -1 : this.detour.ordinal());
        dest.writeInt(this.schedule == null ? -1 : this.schedule.ordinal());
        dest.writeString(this.comment);
        dest.writeInt(this.luggage == null ? -1 : this.luggage.ordinal());
        dest.writeParcelable(this.car, 0);
        dest.writeInt(this.viaggioRosa ? 1 : 0);
        dest.writeInt(this.freeway ? 1 : 0);
        dest.writeLong(lastVisitDate != null ? lastVisitDate.getTime() : -1);
        dest.writeByte(contacted ? (byte) 1 : (byte) 0);
        dest.writeInt(this.bookingMode == null ? -1 : this.bookingMode.ordinal());
        dest.writeInt(this.answerDelay);
        dest.writeString(this.permanentId);
        dest.writeLong(this.departureDate != null ? this.departureDate.getTime() : -1);
        dest.writeParcelable(this.departurePlace, 0);
        dest.writeParcelable(this.arrivalPlace, 0);
        dest.writeString(this.tripOfferEncryptedId);
        dest.writeInt(getBookingType().ordinal());
        dest.writeParcelable(this.userSeat, 0);
        dest.writeByte(isComfort ? (byte) 1 : (byte) 0);
        dest.writeString(messagingStatus);
    }

    public Trip(TripBase other) {
        super(other);
    }

    @SuppressWarnings("WrongConstant")
    private Trip(Parcel in) {
        this.price = in.readParcelable(Price.class.getClassLoader());
        this.user = in.readParcelable(User.class.getClassLoader());
        this.seatsLeft = (Integer) in.readValue(Integer.class.getClassLoader());
        this.stopOvers = new ArrayList<>();
        in.readTypedList(stopOvers, StopOver.CREATOR);
        this.distance = in.readParcelable(Measure.class.getClassLoader());
        this.duration = in.readParcelable(Measure.class.getClassLoader());
        int tmpDetour = in.readInt();
        this.detour = tmpDetour == -1 ? null : Detour.values()[tmpDetour];
        int tmpSchedule = in.readInt();
        this.schedule = tmpSchedule == -1 ? null : Schedule.values()[tmpSchedule];
        this.comment = in.readString();
        int tmpLuggage = in.readInt();
        this.luggage = tmpLuggage == -1 ? null : LuggageSize.values()[tmpLuggage];
        this.car = in.readParcelable(Car.class.getClassLoader());
        this.viaggioRosa = in.readInt() == 1;
        this.freeway = in.readInt() == 1;
        long tmpLastVisitDate = in.readLong();
        this.lastVisitDate = tmpLastVisitDate == -1 ? null : new Date(tmpLastVisitDate);
        this.contacted = in.readByte() != 0;
        int tmpBookingMode = in.readInt();
        this.bookingMode = tmpBookingMode == -1 ? null : ModeList.values()[tmpBookingMode];
        this.answerDelay = in.readInt();
        this.permanentId = in.readString();
        long tmpDepartureDate = in.readLong();
        this.departureDate = tmpDepartureDate == -1 ? null : new Date(tmpDepartureDate);
        this.departurePlace = in.readParcelable(Place.class.getClassLoader());
        this.arrivalPlace = in.readParcelable(Place.class.getClassLoader());
        this.tripOfferEncryptedId = in.readString();
        this.bookingType = BookingType.values()[in.readInt()];
        this.userSeat = in.readParcelable(SeatBooking.class.getClassLoader());
        this.isComfort = in.readByte() != 0;
        this.messagingStatus = in.readString();
    }

    public enum Detour {
        NONE, FIFTEEN_MINUTES, THIRTY_MINUTES, WHATEVER_IT_TAKES
    }

    public enum Schedule {
        ON_TIME, FIFTEEN_MINUTES, THIRTY_MINUTES, ONE_HOUR, TWO_HOURS
    }

    public enum LuggageSize {
        SMALL, MIDDLE, BIG
    }

    public enum ModeList {
        @SerializedName("manual")MANUAL("manual"),
        @SerializedName("auto")AUTO("auto"),
        @SerializedName("none")NONE("none");

        private final String mode;

        private ModeList(String mode) {
            this.mode = mode;
        }

        public boolean equalsName(String otherName) {
            return (otherName == null) ? false : mode.equals(otherName);
        }

        public String toString() {
            return mode;
        }
    }

    @ApiEnumFallback public enum BookingType {
        @SerializedName("online")ONLINE,
        @SerializedName("onboard")ONBOARD,
        @SerializedName("no_booking")NO_BOOKING,
        UNKNOWN
    }
}
