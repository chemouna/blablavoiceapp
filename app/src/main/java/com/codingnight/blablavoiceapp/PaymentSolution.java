package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public final class PaymentSolution implements Parcelable {
    public static final PaymentSolution SIMPLE_SIMPLE = new PaymentSolution(1, "simple");
    public static final PaymentSolution PAYLINE_CREDIT_CARD = new PaymentSolution(2, "credit-card");
    public static final PaymentSolution ADYEN_CREDIT_CARD =
        new PaymentSolution(4, "adyen_credit_card");
    public static final PaymentSolution ADYEN_CSE_EN_GBP =
        new PaymentSolution(6, "adyen-cse-en-gbp");
    public static final PaymentSolution ADYEN_PL_CREDIT_CARD =
        new PaymentSolution(8, "adyen-cse-pl-pln");
    public static final PaymentSolution PAYPAL_FR_EUR =
        new PaymentSolution(9, "paypal-paypal-fr-eur-account");
    public static final PaymentSolution PAYPAL_GB_GBP =
        new PaymentSolution(10, "paypal-paypal-uk-gbp-account");
    public static final PaymentSolution PAYPAL_IT_EUR =
        new PaymentSolution(11, "paypal-paypal-it-eur-account");
    public static final PaymentSolution ADYEN_CSE_IT_EUR =
        new PaymentSolution(12, "adyen-cse-it-eur");
    public static final PaymentSolution ADYEN_CSE_NL_EUR =
        new PaymentSolution(13, "adyen-cse-nl-eur");
    public static final PaymentSolution PAYPAL_NL_EUR =
        new PaymentSolution(14, "paypal-paypal-nl-eur-account");
    public static final PaymentSolution NO_PAYMENT = new PaymentSolution(15, "no-payment");
    public static final PaymentSolution PAYPAL_ES_EUR =
        new PaymentSolution(16, "paypal-paypal-es-eur-account");
    public static final PaymentSolution PAYPAL_PT_EUR =
        new PaymentSolution(17, "paypal-paypal-pt-eur-account");
    public static final PaymentSolution PAYPAL_DE_EUR =
        new PaymentSolution(19, "paypal-paypal-de-eur-account");
    public static final PaymentSolution ADYEN_CSE_DE_EUR =
        new PaymentSolution(20, "adyen-cse-de-eur");
    public static final PaymentSolution ADYEN_CSE_BE_CREDIT_CARD =
        new PaymentSolution(21, "adyen-cse-be-eur");
    public static final PaymentSolution PAYPAL_BE_EUR =
        new PaymentSolution(22, "paypal-be-eur-account");

    public static final int NO_PAYMENT_SOLUTION_DEFINED = 0;
    public static final int ONE_CLICK_PAYMENT_SOLUTION_ID = 2;
    public static final Creator<PaymentSolution> CREATOR = new Creator<PaymentSolution>() {
        public PaymentSolution createFromParcel(Parcel source) {
            return new PaymentSolution(source);
        }

        public PaymentSolution[] newArray(int size) {
            return new PaymentSolution[size];
        }
    };
    private static final PaymentSolution[] ALL = new PaymentSolution[] {
        SIMPLE_SIMPLE, PAYLINE_CREDIT_CARD, ADYEN_CREDIT_CARD, ADYEN_CSE_EN_GBP,
        ADYEN_PL_CREDIT_CARD, PAYPAL_FR_EUR, PAYPAL_GB_GBP, PAYPAL_IT_EUR, ADYEN_CSE_IT_EUR,
        ADYEN_CSE_NL_EUR, PAYPAL_NL_EUR, NO_PAYMENT, PAYPAL_ES_EUR, PAYPAL_PT_EUR, PAYPAL_DE_EUR,
        ADYEN_CSE_DE_EUR, ADYEN_CSE_BE_CREDIT_CARD, PAYPAL_BE_EUR
    };
    private static final PaymentSolution[] PAYPAL = new PaymentSolution[] {
        PAYPAL_FR_EUR, PAYPAL_GB_GBP, PAYPAL_IT_EUR, PAYPAL_NL_EUR, PAYPAL_ES_EUR, PAYPAL_PT_EUR,
        PAYPAL_DE_EUR, PAYPAL_BE_EUR
    };
    private static final PaymentSolution[] ADYEN = new PaymentSolution[] {
        ADYEN_CREDIT_CARD, ADYEN_CSE_EN_GBP, ADYEN_PL_CREDIT_CARD, ADYEN_CSE_IT_EUR,
        ADYEN_CSE_NL_EUR, ADYEN_CSE_DE_EUR, ADYEN_CSE_BE_CREDIT_CARD
    };
    @SerializedName("id") private int id;
    @SerializedName("label") private String label;
    @SerializedName("methods") private List<PaymentSolutionMethods> methodsList;

    public PaymentSolution(int id, String label) {
        this.id = id;
        this.label = label;
        this.methodsList = new ArrayList<>();
    }

    public PaymentSolution(int id, String label, List<PaymentSolutionMethods> methods) {
        this.id = id;
        this.label = label;
        this.methodsList = methods;
    }

    private PaymentSolution(Parcel in) {
        this.id = in.readInt();
        this.label = in.readString();
        methodsList = in.readArrayList(PaymentSolutionMethods.class.getClassLoader());
    }

    public static boolean isSupported(int id) {
        return match(id, ALL);
    }

    public static boolean isPayPal(int id) {
        return match(id, PAYPAL);
    }

    public static boolean isAdyen(int id) {
        return match(id, ADYEN);
    }

    public static boolean isPayline(int id) {
        return id == PAYLINE_CREDIT_CARD.getId();
    }

    public static boolean isNoPayment(int id) {
        return id == NO_PAYMENT.getId() || NO_PAYMENT_SOLUTION_DEFINED == id;
    }

    public static boolean isSimpleSimple(int id) {
        return id == SIMPLE_SIMPLE.getId();
    }

    private static boolean match(int id, PaymentSolution[] solutions) {
        for (PaymentSolution solution : solutions) {
            if (solution.getId() == id) {
                return true;
            }
        }

        return false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<PaymentSolutionMethods> getMethodsList() {
        return methodsList;
    }

    public void setMethodsList(List<PaymentSolutionMethods> methodsList) {
        this.methodsList = methodsList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.label);
        dest.writeList(this.methodsList);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof PaymentSolution) {
            PaymentSolution solution = (PaymentSolution) o;
            return this.getId() == solution.getId();
        } else {
            return super.equals(o);
        }
    }

    @Override
    public String toString() {
        return "id: " + id + ", label: " + label;
    }

    public PaymentSolutionMethods getSavedCreditCardPaymentSolution() {
        if (methodsList != null) {
            for (PaymentSolutionMethods paymentSolutionMethods : methodsList) {
                if (paymentSolutionMethods.isSavedCreditCard()) {
                    return paymentSolutionMethods;
                }
            }
        }
        return null;
    }

    public boolean isSupported() {
        return isSupported(id);
    }

    public boolean isPayPal() {
        return isPayPal(id);
    }

    public boolean isAdyen() {
        return isAdyen(id);
    }

    public boolean isPayline() {
        return isPayline(id);
    }

    public boolean isNoPayment() {
        return isNoPayment(id);
    }

    public boolean isSimpleSimple() {
        return isSimpleSimple(id);
    }
}
