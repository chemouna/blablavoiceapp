package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class OAuth2Information implements Parcelable {

    public static final Creator<OAuth2Information> CREATOR = new Creator<OAuth2Information>() {
        public OAuth2Information createFromParcel(Parcel source) {
            return new OAuth2Information(source);
        }

        public OAuth2Information[] newArray(int size) {
            return new OAuth2Information[size];
        }
    };
    private Type type;
    private String accessToken;

    public OAuth2Information(Type type, String accessToken) {
        this.type = type;
        this.accessToken = accessToken;
    }

    private OAuth2Information(Parcel in) {
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : Type.values()[tmpType];
        this.accessToken = in.readString();
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
        dest.writeString(this.accessToken);
    }

    public enum Type {
        vkontakte,
        facebook
    }
}

