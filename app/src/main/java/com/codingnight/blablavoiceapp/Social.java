package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class Social implements Parcelable, Serializable {

    public static final Creator<Social> CREATOR = new Creator<Social>() {
        @Override
        public Social createFromParcel(Parcel in) {
            return new Social(in);
        }

        @Override
        public Social[] newArray(int size) {
            return new Social[size];
        }
    };
    private int idUser;
    private SocialConnectionStatuses connectionStatuses;

    protected Social(Parcel in) {
        this.idUser = in.readInt();
        this.connectionStatuses =
            in.readParcelable(SocialConnectionStatuses.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.idUser);
        dest.writeParcelable(this.connectionStatuses, 0);
    }

    public int getIdUser() {
        return idUser;
    }

    public SocialConnectionStatuses getConnectionStatuses() {
        return connectionStatuses;
    }
}

