package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class SocialConnectionStatuses implements Parcelable, Serializable {
    public static final Creator<SocialConnectionStatuses> CREATOR =
        new Creator<SocialConnectionStatuses>() {
            @Override
            public SocialConnectionStatuses createFromParcel(Parcel in) {
                return new SocialConnectionStatuses(in);
            }

            @Override
            public SocialConnectionStatuses[] newArray(int size) {
                return new SocialConnectionStatuses[size];
            }
        };
    private SocialConnection linkedin;

    protected SocialConnectionStatuses(Parcel in) {
        this.linkedin = in.readParcelable(SocialConnection.class.getClassLoader());
    }

    public SocialConnection getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(SocialConnection linkedin) {
        this.linkedin = linkedin;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.linkedin, 0);
    }
}
