package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Place implements Parcelable {

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        public Place createFromParcel(Parcel source) {
            return new Place(source);
        }

        public Place[] newArray(int size) {
            return new Place[size];
        }
    };
    @Nullable private String cityName;
    @Nullable private String address;
    private double latitude;
    private double longitude;
    @Nullable private String countryCode;

    public Place(@Nullable String cityName, @Nullable String address, double latitude,
        double longitude, @Nullable String countryCode) {
        this.cityName = cityName;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.countryCode = countryCode;
    }

    public Place(@NonNull Geocode.Result geocodeDetails) {
        this.cityName = null != geocodeDetails.getLocality() ? geocodeDetails.getLocality()
            : geocodeDetails.getFormattedAddress();
        this.address = geocodeDetails.getFormattedAddress();

        if (geocodeDetails.getLocation() != null) {
            this.latitude = geocodeDetails.getLocation().getLat();
            this.longitude = geocodeDetails.getLocation().getLng();
        }

        this.countryCode = geocodeDetails.getCountryCode();
    }

    public Place(@Nullable String cityName) {
        this.cityName = cityName;
    }

    protected Place(Parcel in) {
        this.cityName = in.readString();
        this.address = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.countryCode = in.readString();
    }

    public static Place valueOf(Geocode.Result geocodeDetails) {
        return new Place(geocodeDetails);
    }

    public static Geocode.Result toGeocode(Place place) {
        return toGeocode(place, false);
    }

    public static Geocode.Result toGeocode(Place place, boolean onlyCity) {
        return new Geocode.Result(onlyCity ? place.cityName : place.address, place.latitude,
            place.longitude, place.countryCode, place.address);
    }

    public static String getCityName(Place place) {
        return place == null ? null : place.getCityName();
    }

    @Nullable
    public String getCityName() {
        return cityName;
    }

    public void setCityName(@NonNull String cityName) {
        this.cityName = cityName;
    }

    @Nullable
    public String getAddress() {
        return address;
    }

    public void setAddress(@NonNull String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getLatLon() {
        return String.format("%s,%s", latitude, longitude);
    }

    @Nullable
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(@NonNull String countryCode) {
        this.countryCode = countryCode;
    }

    public boolean isSameCityName(Place place) {
        return place != null && place.getCityName() != null && place.getCityName().equals(cityName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.cityName);
        dest.writeString(this.address);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeString(this.countryCode);
    }
}