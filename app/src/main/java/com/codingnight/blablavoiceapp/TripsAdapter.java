package com.codingnight.blablavoiceapp;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    private Resources res;
    private List<Trip> trips;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView time;
        public TextView price;
        public TextView fromTo;
        public TextView seatsLeft;

        public ViewHolder(View itemView) {
            super(itemView);

            date = (TextView)itemView.findViewById(R.id.search_card_trip_date);
            time = (TextView)itemView.findViewById(R.id.search_card_trip_time);
            price = (TextView)itemView.findViewById(R.id.search_card_trip_price);
            fromTo = (TextView)itemView.findViewById(R.id.search_card_trip_info_from_to);
            seatsLeft = (TextView)itemView.findViewById(R.id.search_card_trip_info_seats_left);
        }
    }

    public TripsAdapter(Context ctx, List<Trip> trips) {
        this.trips = trips;
        this.res = ctx.getResources();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_card_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Trip trip = trips.get(position);

        StringBuilder sbLocation = new StringBuilder();
        List<String> locationsToDisplay = trip.getLocationsToDisplay();

        for (int i = 0; i < locationsToDisplay.size(); i++) {
            if (i > 0)
                sbLocation.append(" » ");
            sbLocation.append(locationsToDisplay.get(i));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
        SimpleDateFormat simpleTimeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        int seatsLeft = trip.getSeatsLeft() != null ? trip.getSeatsLeft() : 0;

        holder.date.setText(simpleDateFormat.format(trip.getDepartureDate()));
        holder.time.setText(simpleTimeFormat.format(trip.getDepartureDate()));
        holder.price.setText(trip.getPrice().getStringValue());
        holder.seatsLeft.setText(String.format(Locale.getDefault(), "%d seats left", seatsLeft));
        if (Build.VERSION.SDK_INT < 23)
            holder.seatsLeft.setTextColor(res.getColor(seatsLeft == 0 ?
                    android.R.color.holo_red_dark : android.R.color.holo_green_dark));
        else
            holder.seatsLeft.setTextColor(res.getColor(seatsLeft == 0 ?
                    android.R.color.holo_red_dark : android.R.color.holo_green_dark, null));
        holder.fromTo.setText(sbLocation.toString());
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

}
