package com.codingnight.blablavoiceapp;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

/**
 * Created by m.cheikhna on 20/06/2016.
 */

public class BlablavoiceApp extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
