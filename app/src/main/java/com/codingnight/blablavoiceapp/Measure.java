package com.codingnight.blablavoiceapp;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.DateUtils;
import java.io.Serializable;

public class Measure implements Parcelable, Serializable {
    public static final String METRIC_LENGTH = "km";
    public static final String IMPERIAL_LENGTH = "mile";

    public static final double METRIC_TO_IMPERIAL = 0.621371192;
    public static final Creator<Measure> CREATOR = new Creator<Measure>() {
        public Measure createFromParcel(Parcel source) {
            return new Measure(source);
        }

        public Measure[] newArray(int size) {
            return new Measure[size];
        }
    };
    private int value;
    private String unity;

    public Measure() {
    }

    public Measure(int value, String unity) {
        this.value = value;
        this.unity = unity;
    }

    private Measure(Parcel in) {
        this.value = in.readInt();
        this.unity = in.readString();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

    public Measure toImperial() {
        if (unity.equals(METRIC_LENGTH)) {
            this.value = (int) (this.value * METRIC_TO_IMPERIAL);
            this.unity = IMPERIAL_LENGTH;
        }

        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.value);
        dest.writeString(this.unity);
    }
}