package com.codingnight.blablavoiceapp;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Car implements Parcelable, Serializable {

    public static final int MIN_CAPACITY = 1;
    public static final int MAX_CAPACITY = 9;
    public static final int DEFAULT_CAPACITY = 4;
    public static final Parcelable.Creator<Car> CREATOR = new Parcelable.Creator<Car>() {
        public Car createFromParcel(Parcel source) {
            return new Car(source);
        }

        public Car[] newArray(int size) {
            return new Car[size];
        }
    };
    private String id;
    private String make;
    private String model;
    private @SerializedName("comfort") String comfortText;
    private Integer comfortNbStar;
    private String color;
    private String colorHexa;
    private String picture;
    @Nullable @Moderation.Status private String pictureModerationStatus;
    @Nullable private Category category = Category.UNKNOWN;
    private Integer numberOfSeat;
    public Car() {
    }
    public Car(Car other) {
        this.id = other.id;
        this.make = other.make;
        this.model = other.model;
        this.comfortText = other.comfortText;
        this.comfortNbStar = other.comfortNbStar;
        this.color = other.color;
        this.colorHexa = other.colorHexa;
        this.picture = other.picture;
        this.pictureModerationStatus = other.pictureModerationStatus;
        this.category = other.category;
        this.numberOfSeat = other.numberOfSeat;
    }

    @SuppressWarnings("ResourceType")
    private Car(Parcel in) {
        this.id = in.readString();
        this.make = in.readString();
        this.model = in.readString();
        this.comfortText = in.readString();
        this.comfortNbStar = (Integer) in.readValue(Integer.class.getClassLoader());
        this.color = in.readString();
        this.colorHexa = in.readString();
        this.picture = in.readString();
        this.pictureModerationStatus = in.readString();
        this.category = Category.values()[in.readInt()];
        this.numberOfSeat = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static List<String> getSeatCountValues() {
        List<String> result = new ArrayList<String>();

        for (int i = MIN_CAPACITY; i <= MAX_CAPACITY; i++) {
            result.add(String.valueOf(i));
        }

        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setComfort(Context context, Comfort comfort) {
        if (comfort != null) {
            comfortText = comfort.name(context);
            comfortNbStar = comfort.nbStars();
        } else {
            comfortText = null;
            comfortNbStar = null;
        }
    }

    public String getComfortText() {
        return comfortText;
    }

    public void setComfortText(String comfortText) {
        this.comfortText = comfortText;
    }

    public Integer getComfortNbStar() {
        return comfortNbStar;
    }

    public void setComfortNbStar(Integer comfortNbStar) {
        this.comfortNbStar = comfortNbStar;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColorHexa() {
        return colorHexa;
    }

    public void setColorHexa(String colorHexa) {
        this.colorHexa = colorHexa;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setPictureModerationStatus(@Nullable @Moderation.Status String pictureModerationStatus) {
        this.pictureModerationStatus = pictureModerationStatus;
    }

    public Integer getNumberOfSeat() {
        return numberOfSeat;
    }

    public void setNumberOfSeat(Integer numberOfSeat) {
        this.numberOfSeat = numberOfSeat;
    }

    @NonNull
    public Category getCategory() {
        return category != null ? category : Category.UNKNOWN;
    }

    public void setCategory(@Nullable Category category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.make);
        dest.writeString(this.model);
        dest.writeString(this.comfortText);
        dest.writeValue(this.comfortNbStar);
        dest.writeString(this.color);
        dest.writeString(this.colorHexa);
        dest.writeString(this.picture);
        dest.writeString(this.pictureModerationStatus);
        dest.writeInt(getCategory().ordinal());
        dest.writeValue(this.numberOfSeat);
    }

    @ApiEnumFallback public enum Category {
        @SerializedName("_UE_BERLINE")BERLINE(R.string.global_car_category_berline),
        @SerializedName("_UE_TOURISM")TOURISM(R.string.global_car_category_tourism),
        @SerializedName("_UE_CABRIOLET")CABRIOLET(R.string.global_car_category_cabriolet),
        @SerializedName("_UE_BREAK")BREAK(R.string.global_car_category_break),
        @SerializedName("_UE_44")_44(R.string.global_car_category_44),
        @SerializedName("_UE_COMPANY")COMPANY(R.string.global_car_category_company),
        @SerializedName("_UE_VAN")VAN(R.string.global_car_category_van),
        @SerializedName("_UE_SMALL_UTILITY")SMALL_UTILITY(
            R.string.global_car_category_small_utility),
        @SerializedName("_UE_BIG_UTILITY")BIG_UTILITY(R.string.global_car_category_big_utility),
        @SerializedName("_UE_CYCLE")CYCLE(R.string.global_car_category_cycle),
        UNKNOWN(R.string.global_car_category_tourism);

        private final int extStringId;

        Category(int extStringId) {
            this.extStringId = extStringId;
        }

        public static List<String> names(Context context) {
            Category[] values = values();
            List<String> names = new ArrayList<String>();

            for (Category value : values) {
                names.add(value.name(context));
            }

            return names;
        }

        public static Category fromName(Context context, String name) {
            for (Category category : Category.values()) {
                if (category.name(context).equals(name)) {
                    return category;
                }
            }
            return null;
        }

        public String name(Context context) {
            return context.getString(extStringId);
        }
    }

    public enum Comfort {
        @SerializedName("_CO_BASIC")BASIC(R.string.global_car_comfort_basic, 1),
        @SerializedName("_CO_NORMAL")NORMAL(R.string.global_car_comfort_normal, 2),
        @SerializedName("_CO_COMFORT")COMFORT(R.string.global_car_comfort_comfort, 3),
        @SerializedName("_CO_LUXE")LUXE(R.string.global_car_comfort_luxe, 4);

        private final int extStringId;
        private final int nbStars;

        Comfort(int extStringId, int nbStars) {
            this.extStringId = extStringId;
            this.nbStars = nbStars;
        }

        public static List<String> names(Context context) {
            Comfort[] values = values();
            List<String> names = new ArrayList<String>();

            for (Comfort value : values) {
                names.add(value.name(context));
            }

            return names;
        }

        public static Comfort fromName(Context context, String name) {
            for (Comfort comfort : Comfort.values()) {
                if (comfort.name(context).equals(name)) {
                    return comfort;
                }
            }
            return null;
        }

        public String name(Context context) {
            return context.getString(extStringId);
        }

        public int nbStars() {
            return nbStars;
        }
    }
}