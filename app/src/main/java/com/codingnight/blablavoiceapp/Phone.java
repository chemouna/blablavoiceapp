package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class Phone implements Parcelable, Serializable {

    public static final String UNAVAILABLE_REASON_HIDDEN = "phone_hidden";
    public static final String UNAVAILABLE_REASON_NO_PHONE = "no_phone";
    public static final String UNAVAILABLE_REASON_NEED_LOG_IN = "need_log_in";
    public static final String UNAVAILABLE_REASON_ON_HOLD = "phone_on_hold";
    public static final Creator<Phone> CREATOR = new Creator<Phone>() {
        public Phone createFromParcel(Parcel source) {
            return new Phone(source);
        }

        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };
    @Nullable private final Integer countryCode;
    @Nullable private final String nationalNumber;
    @Nullable private final String regionCode;
    @Nullable @UnavailableReason private final String unavailableReason;
    @Nullable private final String rawInput;

    public Phone() {
        this.countryCode = null;
        this.nationalNumber = null;
        this.regionCode = null;
        this.unavailableReason = null;
        this.rawInput = null;
    }

    public Phone(@Nullable Integer countryCode, @Nullable String nationalNumber,
        @Nullable String regionCode, @Nullable @UnavailableReason String unavailableReason,
        @Nullable String rawInput) {
        this.countryCode = countryCode;
        this.nationalNumber = nationalNumber;
        this.regionCode = regionCode;
        this.unavailableReason = unavailableReason;
        this.rawInput = rawInput;
    }

    @SuppressWarnings("ResourceType")
    private Phone(Parcel in) {
        this.countryCode = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nationalNumber = in.readString();
        this.regionCode = in.readString();
        this.rawInput = in.readString();
        this.unavailableReason = in.readString();
    }

    public boolean hasValidNumber() {
        return countryCode != null && nationalNumber != null;
    }

    @Nullable
    public Integer getCountryCode() {
        return countryCode;
    }

    @Nullable
    public String getRegionCode() {
        return regionCode;
    }

    @Nullable
    public String getNationalNumber() {
        return nationalNumber;
    }

    @Nullable
    @UnavailableReason
    public String getUnavailableReason() {
        return unavailableReason;
    }

    public boolean isPhoneUnavailable() {
        return this.unavailableReason != null;
    }

    @Nullable
    public String getRawInput() {
        return rawInput;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.countryCode);
        dest.writeString(this.nationalNumber);
        dest.writeString(this.regionCode);
        dest.writeString(this.rawInput);
        dest.writeString(this.unavailableReason);
    }

    @Override
    public String toString() {
        return hasValidNumber() ? String.format("+%d %s", countryCode, nationalNumber) : "";
    }

    @Retention(RetentionPolicy.SOURCE) @StringDef({
        UNAVAILABLE_REASON_HIDDEN, UNAVAILABLE_REASON_NO_PHONE, UNAVAILABLE_REASON_NEED_LOG_IN,
        UNAVAILABLE_REASON_ON_HOLD
    }) public @interface UnavailableReason {
    }
}

