package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;

public class Price implements Parcelable {

    public static final Parcelable.Creator<Price> CREATOR = new Creator<Price>() {
        public Price createFromParcel(Parcel source) {
            return new Price(source);
        }

        public Price[] newArray(int size) {
            return new Price[size];
        }
    };
    private float value;
    private String symbol;
    private String currency;
    private PriceColor priceColor;
    private String stringValue;

    public Price() {
    }

    private Price(Parcel in) {
        this.value = in.readFloat();
        this.symbol = in.readString();
        this.currency = in.readString();
        int tmpPriceColor = in.readInt();
        this.priceColor = tmpPriceColor == -1 ? null : PriceColor.values()[tmpPriceColor];
        this.stringValue = in.readString();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public PriceColor getPriceColor() {
        return priceColor;
    }

    public void setPriceColor(PriceColor priceColor) {
        this.priceColor = priceColor;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String toApiString() {
        return String.format("%s %s", value, currency);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(this.value);
        dest.writeString(this.symbol);
        dest.writeString(this.currency);
        dest.writeInt(this.priceColor == null ? -1 : this.priceColor.ordinal());
        dest.writeString(this.stringValue);
    }

    public enum PriceColor {
        GREEN, ORANGE, RED
    }
}