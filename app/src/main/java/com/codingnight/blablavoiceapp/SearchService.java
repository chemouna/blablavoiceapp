package com.codingnight.blablavoiceapp;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;

public class SearchService {

    public static void getTrips(String request, Callback<FindRequestResults> callback) {
        Call<FindRequestResults> call = BlaBlaVoiceApiService.getApi().trips(request);
        call.enqueue(callback);
    }

    public static void parse(String request, Callback<List<ParseResult>> callback) {
        Call<List<ParseResult>> call = BlaBlaVoiceApiService.getApi().parse(request);
        call.enqueue(callback);
    }

}
