package com.codingnight.blablavoiceapp;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;

public class UserBase implements Parcelable, Serializable {
    public static final Creator<UserBase> CREATOR = new Creator<UserBase>() {
        @Override
        public UserBase createFromParcel(Parcel in) {
            return new UserBase(in);
        }

        @Override
        public UserBase[] newArray(int size) {
            return new UserBase[size];
        }
    };
    protected String displayName;

    public UserBase() {
    }

    public UserBase(String displayName) {
        this.displayName = displayName;
    }

    protected UserBase(Parcel in) {
        this.displayName = in.readString();
    }

    public UserBase(UserBase other) {
        this.displayName = other.displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.displayName);
    }
}